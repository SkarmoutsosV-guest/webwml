# organization webwml Catalan template.
# Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004-2011, 2017-2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2020-12-20 19:14+0100\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "correu de delegació"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "correu de nomenament"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegat"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>ell"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>ella"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegació"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>ells/elles"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "actual"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "membre"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "gestor"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Administrador de llançament de la versió estable"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "mag"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "president"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secretari"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "representants"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "rol"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"A la llista següent, <q>actual</q> s'empra per a posicions temporals "
"(escollides o nomenades amb una data de caducitat determinada)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Directors"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribució"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr "Comunicació i divulgació"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr "Equip de protecció de dades"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
msgid "Publicity team"
msgstr "Equip de publicitat"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr "Afiliació en altres organitzacions"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:305
msgid "Support and Infrastructure"
msgstr "Suport i infraestructura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Comitè tècnic"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Secretari"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Projectes de desenvolupament"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Arxius FTP"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Mestres de FTP"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Assistents de FTP"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Mags de FTP"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Gestió de llançaments"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Equip de llançament"

#: ../../english/intro/organization.data:148
msgid "Quality Assurance"
msgstr "Control de qualitat"

#: ../../english/intro/organization.data:149
msgid "Installation System Team"
msgstr "Equip del sistema d'instal·lació"

#: ../../english/intro/organization.data:150
msgid "Debian Live Team"
msgstr "Equip del sistema autònom de Debian"

#: ../../english/intro/organization.data:151
msgid "Release Notes"
msgstr "Notes de la versió"

#: ../../english/intro/organization.data:153
msgid "CD/DVD/USB Images"
msgstr "Imatges de CD/DVD/USB"

#: ../../english/intro/organization.data:155
msgid "Production"
msgstr "Producció"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Proves"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr "Equip en núvol"

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Infraestructura d'autocompilació"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Equip de wanna-build"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Administració dels dimonis de compilació"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Documentació"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Llista de paquets en perspectiva o en falta de feina"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Contacte de premsa"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Pàgines web"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Divulgació"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Projecte Debian dona"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr "Comunitat"

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Per enviar missatges privats a tot l'Equip de Comunitat, empreu la clau "
"OpenPGP <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Esdeveniments"

#: ../../english/intro/organization.data:259
msgid "DebConf Committee"
msgstr "Comitè de DebConf"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Programa de socis"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Coordinació de donacions de maquinari"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr "Fundació GNOME"

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr "Institut Professional Linux (LPI)"

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr "Linux Standards Base (LSB)"

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr "Free Standards Group (FSG)"

#: ../../english/intro/organization.data:292
msgid "SchoolForge"
msgstr "SchoolForge"

#: ../../english/intro/organization.data:295
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organització per a l'avenç d'estàndards d'informació estructurada"

#: ../../english/intro/organization.data:298
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr "OVAL: Llenguatge obert d'avaluació de vulnerabilitats"

#: ../../english/intro/organization.data:301
msgid "Open Source Initiative"
msgstr "Iniciativa de codi obert (OSI)"

#: ../../english/intro/organization.data:308
msgid "Bug Tracking System"
msgstr "Sistema de Seguiment d'Errors"

#: ../../english/intro/organization.data:313
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administració i arxius de les llistes de correu"

#: ../../english/intro/organization.data:321
msgid "New Members Front Desk"
msgstr "Recepció de nous membres"

#: ../../english/intro/organization.data:327
msgid "Debian Account Managers"
msgstr "Administradors dels comptes de Debian"

#: ../../english/intro/organization.data:331
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Per enviar un missatge privat al grup de DAMs, empreu la clau OpenPGP "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:332
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantenidors de l'anell de claus (OpenPGP)"

#: ../../english/intro/organization.data:336
msgid "Security Team"
msgstr "Equip de seguretat"

#: ../../english/intro/organization.data:347
msgid "Policy"
msgstr "Política"

#: ../../english/intro/organization.data:350
msgid "System Administration"
msgstr "Administració del sistema"

#: ../../english/intro/organization.data:351
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Aquesta és l'adreça a utilitzar quan es troben problemes en una màquina de "
"Debian, incloent problemes amb contrasenyes o si necessiteu la instal·lació "
"d'un paquet."

#: ../../english/intro/organization.data:361
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Si teniu problemes de hardware en màquines de Debian, mireu la pàgina de <a "
"href=\"https://db.debian.org/machines.cgi\">Màquines de Debian</a>, hauria "
"de contenir informació de l'administrador de cada màquina."

#: ../../english/intro/organization.data:362
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador del directori LDAP de desenvolupadors"

#: ../../english/intro/organization.data:363
msgid "Mirrors"
msgstr "Rèpliques"

#: ../../english/intro/organization.data:369
msgid "DNS Maintainer"
msgstr "Mantenidor del DNS"

#: ../../english/intro/organization.data:370
msgid "Package Tracking System"
msgstr "Sistema de seguiment de paquets"

#: ../../english/intro/organization.data:372
msgid "Treasurer"
msgstr "Tresorers"

#: ../../english/intro/organization.data:379
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Peticions d'us de la <a name=\"trademark\" href=\"m4_HOME/trademark\">marca "
"comercial</a>"

#: ../../english/intro/organization.data:383
msgid "Salsa administrators"
msgstr "Administració de Salsa"

#~ msgid "APT Team"
#~ msgstr "Equip APT"

#~ msgid "Accountant"
#~ msgstr "Contable"

#~ msgid "Alioth administrators"
#~ msgstr "Administració d'Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Anti-assetjament"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "CD Vendors Page"
#~ msgstr "Pàgina de proveïdors de CD"

#~ msgid "Consultants Page"
#~ msgstr "Pàgina de consultors"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribució de Debian adaptada"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux per a la computació d'empreses"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Equip de l'anell de claus dels mantenidors de Debian"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribució multimedia de Debian"

#~ msgid "Debian Pure Blends"
#~ msgstr "Barreges pures de Debian"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian per a l'astronomia"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian per a nens de 1 a 99"

#~ msgid "Debian for education"
#~ msgstr "Debian per a l'educació"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian per a la pràctica i recerca medica"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian per organitzacions sense anim de lucre"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian per a gent amb discapacitats"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian per a la ciència i recerca relacionada"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian a les oficines de dret"

#~ msgid "Embedded systems"
#~ msgstr "Sistemes empotats"

#~ msgid "Firewalls"
#~ msgstr "Tallafocs"

#~ msgid "Handhelds"
#~ msgstr "Ordinadors de ma"

#~ msgid "Individual Packages"
#~ msgstr "Paquets individuals"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Sistema d'instal·lació per a «stable»"

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordinació de signatura de claus"

#~ msgid "Laptops"
#~ msgstr "Portàtils"

#~ msgid "Live System Team"
#~ msgstr "Equip del sistema autònom"

#~ msgid "Mailing List Archives"
#~ msgstr "Arxius de les llistes de correu"

#~ msgid "Marketing Team"
#~ msgstr "Equip de màrqueting"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Els noms individuals dels administradors dels dimonis de compilació es "
#~ "poden trobar a <a href=\"http://www.buildd.net\">http://www.buildd.net</"
#~ "a>. Seleccioneu una arquitecture i una distribució per veure els dimonis "
#~ "de compilació disponibles i els seus administradors."

#~ msgid "Ports"
#~ msgstr "Arquitectures"

#~ msgid "Publicity"
#~ msgstr "Publicitat"

#~ msgid "Release Assistants"
#~ msgstr "Assistents de llançament"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "Assistents de llançaments per a «stable»"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Equip de llançaments per a «stable»"

#~ msgid "Release Wizard"
#~ msgstr "Mag de llançaments"

#~ msgid "Security Audit Project"
#~ msgstr "Projecte d'auditoria de seguretat"

#~ msgid "Special Configurations"
#~ msgstr "Configuracions especials"

#~ msgid "Testing Security Team"
#~ msgstr "Equip de seguretat de Proves"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "El sistema operatiu universal com el seu escriptori"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Els administradors responsables dels dimonis de compilació per a una "
#~ "arquitectura particular, es poden contactar a <genericemail arch@buildd."
#~ "debian.org>, per exemple <genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Aquest encara no és un projecte intern oficial de Debian però ha anunciat "
#~ "la intenció de ser-ho."

#~ msgid "User support"
#~ msgstr "Suport d'usuari"

#~ msgid "Vendors"
#~ msgstr "Venedors"

#~ msgid "Volatile Team"
#~ msgstr "Equip de llançament da la versió volàtil"
