#use wml::debian::template title="Debian &ldquo;bullseye&rdquo; 安装信息" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="08a0f9fd40d72266eb96cec4b5e88f09ccc00215"

<h1>安装 Debian <current_release_bullseye></h1>

<if-stable-release release="bookworm">
<p><strong>Debian 11 已被 \
<a href="../../bookworm/">Debian 12（<q>bookworm</q>）</a>取代。\
以下的某些安装映像可能已不可用，或不能工作，建议您转而安装 bookworm。
</strong></p>
</if-stable-release>

<if-stable-release release="buster">
<p>
For installation images and documentation about how to install <q>bullseye</q>
(which is currently Testing), see
<a href="$(HOME)/devel/debian-installer/">the Debian-Installer page</a>.
</if-stable-release>

<if-stable-release release="bullseye">
<p>
<strong>要安装 Debian</strong> <current_release_bullseye>\
（<em>bullseye</em>），请下载以下映像中的一个（所有 i386 和 amd64 CD/DVD 映\
像均可以在 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]上使用）：
</p>

<div class="line">
<div class="item col50">
	<p><strong>网络安装 CD 映像（通常 150-280 MB）</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>完整 CD 映像集</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>完整 DVD 映像集</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD（通过 <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>）</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD（通过 <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>）</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>蓝光（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>其他映像（网络启动、灵活的 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]，等等）</strong></p>
<other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<div id="firmware_nonfree" class="important">
<p>
如果您的系统的任何硬件的驱动<strong>需要加载非自由固件</strong>\
，您可以使用\
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">\
常见固件软件包的 tar [CN:文件:][HKTW:档案:]</a>中的一个，或者下载包含\
这些<strong>非自由</strong>固件的<strong>非官方</strong>映像。\
如何使用这些 tar [CN:文件:][HKTW:档案:]，以及\
如何在安装过程中加载固件，请参阅<a href="../amd64/ch06s04">安装手册</a>。
</p>
<div class="line">
<div class="item col50">
<p><strong>网络安装（通常 240-290 MB）<strong>含非自由固件的</strong> \
CD 映像</strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>注意</strong>
</p>
<ul>
    <li>下载完整的 CD 和 DVD 映像，推荐使用 BitTorrent 或 jigdo。</li>
    <li>对于不常见的架构，CD 和 DVD 映像集中，只有有限的映像\
提供 ISO [CN:文件:][HKTW:档案:] 或 BitTorrent 下载。\
完整的映像集只能通过 jigdo 下载。</li>
    <li>多架构 <em>CD</em> 映像支持 i386/amd64；\
安装过程与从单架构的网络安装映像安装相似。</li>
    <li>多架构 <em>DVD</em> 映像支持 i386/amd64；\
安装过程与从单架构的完整 CD 映像安装相似；\
DVD 也包含了所有包含的软件包的源代码。</li>
    <li>对于安装映像，校验[CN:文件:][HKTW:档:]（<tt>SHA256SUMS</tt>、\
<tt>SHA512SUMS</tt> 和其他校验和）可以在映像的同一目录下找到。</li>
</ul>


<h1>文档</h1>

<p>
<strong>如果您在安装前只想阅读一份文档</strong>，请阅读我们的\
<a href="../i386/apa">安装指南</a>\
，这是一份安装过程的简要介绍。其他有用的文档包括：
</p>

<ul>
<li><a href="../installmanual">Bullseye 安装手册</a><br />
详细的安装步骤</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian 安装程序 FAQ</a> 和 <a href="$(HOME)/CD/faq/">Debian CD FAQ</a><br />
常见问题和解答</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian 安装程序 Wiki</a><br />
由[CN:社区:][HKTW:社群:]维护的文档</li>
</ul>

<h1 id="errata">勘误</h1>

<p>
这是 Debian <current_release_bullseye> 安装程序的已知问题列表。\
如果您在安装 Debian 时遇到了这里没有列出的问题，请发一份\
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">安装报告</a>给我们，\
描述所遇到的问题，或者\
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">访问维基</a>\
查看其他已知问题。
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">版本 11.0 的勘误</h3>

<dl class="gloss">

<dt>部分声卡需要固件</dt>
<dd>似乎有部分声卡需要加载固件才能\
播放声音。截至 Bullseye，安装程序无法\
很早地加载它们，这意味着对这些声卡而言，\
在安装过程中，语音合成器将不可用。\
一个可能的临时解决方案是\
使用另一张不需要固件的声卡。\
参见<a href="https://bugs.debian.org/992699">这份缺陷报告</a>\
以跟踪该问题的解决进度。</dd>

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
给下个 Debian 发布版本使用的改进版本的安装系统正在开发中，\
它也可用于安装 bullseye。详情请见 \
<a href="$(HOME)/devel/debian-installer/">Debian 安装程序项目主页</a>。
</p>
</if-stable-release>
