#use wml::debian::translation-check translation="b814e5f8f89e2086e558034fbf26a53d5f985512"
<define-tag pagetitle>Handshake donerer 300.000 US-dollar til Debian</define-tag>
<define-tag release_date>2019-03-29</define-tag>
#use wml::debian::news

<p>
I 2018 modtog Debian-projektet en donation på 300.000 US-dollar fra
<a href="https://handshake.org/">Handshake</a>, en organisation som udvikler et 
eksperimentelt peer to peer-roddomænenavnssystem.
</p>

<p>
Dette betydelige økonomiske bidrag hjælper Debian med at fortsætte sin plan for 
hardwareudskiftning, udarbejdet af <a href="https://wiki.debian.org/Teams/DSA">\
Debian System Administrators</a>, med fornyelse af servere og andre 
hardwarekomponenter, som derved gør projektets udviklings- og 
fællesskabsinfrastruktur mere pålidelig.
</p>

<p>
<q><em>Vi takker dybtfølt og oprigtigt Handshake Foundation for deres støtte til 
Debian-projektet,</em></q> sagde Chris Lamb, Debians projektleder.
<q><em>Bidrag som deres gør det muligt for et stort antal forskellige 
bidragydere over hele verden, at samarbejde mod vores fælles mål om at udvikle 
et helt frit <q>universelt</q> styresystem.</em></q>
</p>

<p>
Handshake er en decentraliseret og rettighedsløs navngivningsprotokol, som er 
kompatibel med DNS, hvor alle peers validerer og står for håndteringen af 
rodzonen, med det formål at lave et alternativ til de eksisterende 
certificeringsmyndigheder.
</p>

<p>
Handshake-projektet, dets sponsorer og bidragydere anser fri og open 
source-software som en afgørende del af internettets og deres projekts 
grundelementer, hvorfor de besluttede at geninvestere i fri software, ved at 
give 10.200.000 US-dollar til forskellige FLOSS-udviklere og -projekter, 
foruden nonprofitorganisationer og universiteter, som støtter udvikling af fri 
software.
</p>

<p>
Mange tak for jeres støtte, Handshake!
</p>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
