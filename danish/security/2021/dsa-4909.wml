#use wml::debian::translation-check translation="022e55922f28ad2d0ab4d75ed4d28a027bafddfa" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i BIND, en DNS-server-implementering.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25214">CVE-2021-25214</a>

    <p>Greg Kuechle opdagede at en misdannet indgående IXFR-overførsel kunne 
    udløse en assertionfejl i named, medførende lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25215">CVE-2021-25215</a>

    <p>Siva Kakarla opdagede at named kunne gå ned når en DNAME-post placeret i 
    ANSWER-afsnittet under DNAME-forfølgelse, viste sig at være det endelige 
    svar på en klientforespørgsel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25216">CVE-2021-25216</a>

    <p>Man opdagede at SPNEGO-implementeringen, som anvendes af BIND, var sårbar 
    over for en bufferoverløbssårbarhed.  Denne opdatering skifter til at 
    anvende SPNEGO-implementeringe fra Kerberos-bibliotekerne.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1:9.11.5.P4+dfsg-5.1+deb10u5.</p>

<p>Vi anbefaler at du opgraderer dine bind9-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende bind9, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4909.data"
