#use wml::debian::template title="Informatie over spiegelservers indienen"
#include "$(ENGLISHDIR)/mirror/submit.inc"
#use wml::debian::translation-check translation="cd8f39027a950a5174d2d49109ba6800c0f0cdec"

<p>Indien u informatie wilt indienen over een spiegelserver van Debian, kunt u dit doen met onderstaand formulier. De ingediende informatie zal weergegeven worden in de spiegelserverlijst.
</p>

<p>Merk op dat alle aangemelde spiegelservers de
<a href="ftpmirror">in onze documentatie uiteengezette regels en richtlijnen</a>
moeten volgen. In het bijzonder
</p>
<ul>
<li>moet u <a href="ftpmirror#how">ftpsync</a> gebruiken om Debian te spiegelen,
<li>moet u een goede keuze voor stroomopwaarts maken (<strong>geen</strong>
    (http)-dienstnaam zoals <code>ftp.CC.debian.org</code>,
    geen DNS round robin (DNS-draaideur), geen CDN),
<li>moet u viermaal per dag updaten om aan te sluiten bij de frequentie waarop
    het archief bijgewerkt wordt (of met stroomopwaarts triggers instellen, of
    ieder uur <code>ftpsync-cron</code> gebruiken om stroomopwaarts te
    controleren op wijzigingen en synchronisaties te initiëren). Voorts
<li>moet uw spiegelserver een traceerbestand met een passende naam hebben
    (ftpsync zal dit doen als MIRRORNAME correct is ingesteld),
<li>moet uw spiegelserver zo nodig met het commando touch de vlag-bestanden
    <code>/Archive-Update-in-Progress-XXX</code> en
    <code>/Archive-Update-Required-XXX</code> aanmaken (opnieuw zal ftpsync dat
    in uw plaats doen) om de van u afhankelijke benedenstroomse spiegelservers
    te helpen correct te synchroniseren.
</ul>

<form-action "" archive-upstream https://cgi.debian.org/cgi-bin/submit_mirror.pl>

<h2>Basisinformatie</h2>

<p>
<input type="radio" name="submissiontype" value="new" checked>
Aanmelding van een nieuwe spiegelserver
&nbsp; &nbsp; &nbsp;
<input type="radio" name="submissiontype" value="update">
Een update van een bestaande spiegelserver
</p>

<p>
Spiegelservernaam: <input type="text" name="site" size="30"></p>

<p>Voer in de onderstaande velden de paden naar de Debian-spiegelserver op uw
site in. Laat niet-relevante velden leeg.</p>

<table>
  <tr><td>Pakketarchief over HTTP: </td><td><input type="text" name="archive-http" id="archve-http" size="30" value="/debian" readonly="readonly"> <small>Het archief moet te vinden zijn op <code>/debian</code>.</small></td></tr>
<tablerowdef "Pakketarchief over rsync"  archive-rsync  30 "debian" " <small>Indien u rsync aanbiedt, wordt gesuggereerd <code>debian</code> als modulenaam te gebruiken.</small>">
# <tablerow "CD/DVD images, over HTTP"      cdimage-http   30>
# <tablerow "CD/DVD images, over rsync"     cdimage-rsync  30>
# <tablerow "Old Debian releases, over HTTP"  old-http     30>
# <tablerow "Old Debian releases, over rsync" old-rsync    30>
</table>

<h2>Informatie over de spiegelserversite</h2>

<table>
<tr>
<td>Gespiegelde architecturen:
<td>
<label><input type=checkbox name=architectures id="allarch" value="ALL" onclick="allarches()">&nbsp;<em>allemaal (d.w.z. dat er niet aan uitsluiting per architectuur wordt gedaan)</em></label><br>
<archlist>
</td></tr>
</table>

<table>
<tablerow "Naam van de beheerder van de site"         maint_name    30>
<tablerow "Openbare e-mail van de beheerder van de site"              maint_public_email    30>
<tr><td>Land van de site:  <td><select name="country">
<countrylist>
</select>
<tablerow "Locatie van de site (facultatief)"     location      30>
<tablerow "Naam van de sitesponsor (facultatief)" sponsor_name  30>
<tablerow "URL van de sitesponsor (facultatief)"  sponsor_url   30>
</table>

<table><tr>
<td valign="top">Commentaar:</td>
<td><textarea name="comment" cols=40 rows=7></textarea></td>
</tr></table>

<p><label>Ik heb me aangemeld voor de
<a href="https://lists.debian.org/debian-mirrors-announce/">
mailinglijst voor aankondigingen</a>
<input type="checkbox" name="mlannounce"></label>
</p>

<p>
<input type="submit" value="Indienen"> <input type="reset" value="Formulier leegmaken">
</p>
</form>

<p>Uw site zou binnen een paar weken op de lijst moeten verschijnen, van zodra
een menselijke functionaris deze heeft geverifieerd en opgenomen. We zullen u
een e-mail sturen mochten er problemen zijn met het ingezonden formulier.</p>

<p>Indien u binnen de drie maanden geen enkele reactie ontving, kunt u contact
opnemen met ons op <email mirrors@debian.org>.</p>

