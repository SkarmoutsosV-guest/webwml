<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It is not feasible to fully support some Debian packages through the releases
life cycle. The debian-security-support package provides the
check-support-status tool that helps to warn the administrator about installed
packages whose security support is limited or has to prematurely end.</p>

<p>For Debian 7 <q>Wheezy</q>, debian-security-support version 2016.05.09+nmu1~deb7u1
updates the list of packages with restricted support in Wheezy LTS. In
particular, this version also includes a new feature to notify the user about
oncoming end-of-lifes.</p>

<p>We recommend you to install the debian-security-support and run
check-support-status to verify the status of installed packages. Please, refer
to the check-support-status (1) man page for more information about how to
use it.</p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in debian-security-support version 2016.05.09+nmu1~deb7u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-465.data"
# $Id: $
