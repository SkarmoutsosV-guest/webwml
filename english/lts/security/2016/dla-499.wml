<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8865">CVE-2015-8865</a>
      <p>The file_check_mem function in funcs.c in file before 5.23, as used
      in the Fileinfo component in PHP before 5.5.34, 5.6.x before 5.6.20,
      and 7.x before 7.0.5, mishandles continuation-level jumps, which
      allows context-dependent attackers to cause a denial of service
      (buffer overflow and application crash) or possibly execute arbitrary
      code via a crafted magic file.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8866">CVE-2015-8866</a>
      <p>libxml_disable_entity_loader setting is shared between threads
      ext/libxml/libxml.c in PHP before 5.5.22 and 5.6.x before 5.6.6, when
      PHP-FPM is used, does not isolate each thread from
      libxml_disable_entity_loader changes in other threads, which allows
      remote attackers to conduct XML External Entity (XXE) and XML Entity
      Expansion (XEE) attacks via a crafted XML document, a related issue
      to <a href="https://security-tracker.debian.org/tracker/CVE-2015-5161">CVE-2015-5161</a>.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8878">CVE-2015-8878</a>
      <p>main/php_open_temporary_file.c in PHP before 5.5.28 and 5.6.x before
      5.6.12 does not ensure thread safety, which allows remote attackers to
      cause a denial of service (race condition and heap memory corruption)
      by leveraging an application that performs many temporary-file accesses.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8879">CVE-2015-8879</a>
      <p>The odbc_bindcols function in ext/odbc/php_odbc.c in PHP before 5.6.12
      mishandles driver behavior for SQL_WVARCHAR columns, which allows
      remote attackers to cause a denial of service (application crash) in
      opportunistic circumstances by leveraging use of the odbc_fetch_array
      function to access a certain type of Microsoft SQL Server table.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4070">CVE-2016-4070</a>
      <p>Integer overflow in the php_raw_url_encode function in ext/standard/url.c
      in PHP before 5.5.34, 5.6.x before 5.6.20, and 7.x before 7.0.5 allows
      remote attackers to cause a denial of service (application crash) via a
      long string to the rawurlencode function.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4071">CVE-2016-4071</a>
      <p>Format string vulnerability in the php_snmp_error function in
      ext/snmp/snmp.c in PHP before 5.5.34, 5.6.x before 5.6.20, and 7.x
      before 7.0.5 allows remote attackers to execute arbitrary code via
      format string specifiers in an SNMP::get call.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4072">CVE-2016-4072</a>
      <p>The Phar extension in PHP before 5.5.34, 5.6.x before 5.6.20, and 7.x
      before 7.0.5 allows remote attackers to execute arbitrary code via a
      crafted filename, as demonstrated by mishandling of \0 characters by
      the phar_analyze_path function in ext/phar/phar.c.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4073">CVE-2016-4073</a>
      <p>Multiple integer overflows in the mbfl_strcut function in
      ext/mbstring/libmbfl/mbfl/mbfilter.c in PHP before 5.5.34, 5.6.x before
      5.6.20, and 7.x before 7.0.5 allow remote attackers to cause a denial
      of service (application crash) or possibly execute arbitrary code via
      a crafted mb_strcut call.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4343">CVE-2016-4343</a>
      <p>The phar_make_dirstream function in ext/phar/dirstream.c in PHP before
      5.6.18 and 7.x before 7.0.3 mishandles zero-size ././@LongLink files,
      which allows remote attackers to cause a denial of service
      (uninitialized pointer dereference) or possibly have unspecified other
      impact via a crafted TAR archive.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4537">CVE-2016-4537</a>
      <p>The bcpowmod function in ext/bcmath/bcmath.c in PHP before 5.5.35,
      5.6.x before 5.6.21, and 7.x before 7.0.6 accepts a negative integer
      for the scale argument, which allows remote attackers to cause a
      denial of service or possibly have unspecified other impact via a
      crafted call.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4539">CVE-2016-4539</a>
      <p>The xml_parse_into_struct function in ext/xml/xml.c in PHP before
      5.5.35, 5.6.x before 5.6.21, and 7.x before 7.0.6 allows remote
      attackers to cause a denial of service (buffer under-read and
      segmentation fault) or possibly have unspecified other impact via
      crafted XML data in the second argument, leading to a parser level
      of zero.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4540">CVE-2016-4540</a> /
      <a href="https://security-tracker.debian.org/tracker/CVE-2016-4541">CVE-2016-4541</a>
      <p>The grapheme_strpos function in ext/intl/grapheme/grapheme_string.c
      in before 5.5.35, 5.6.x before 5.6.21, and 7.x before 7.0.6 allows
      remote attackers to cause a denial of service (out-of-bounds read)
      or possibly have unspecified other impact via a negative offset.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4542">CVE-2016-4542</a> /
      <a href="https://security-tracker.debian.org/tracker/CVE-2016-4543">CVE-2016-4543</a> /
      <a href="https://security-tracker.debian.org/tracker/CVE-2016-4544">CVE-2016-4544</a>
      <p>The exif_process_* function in ext/exif/exif.c in PHP before 5.5.35,
      5.6.x before 5.6.21, and 7.x before 7.0.6 does not validate IFD sizes,
      which allows remote attackers to cause a denial of service
      (out-of-bounds read) or possibly have unspecified other impact via
      crafted header data.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in php5 version 5.4.45-0+deb7u3</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-499.data"
# $Id: $
