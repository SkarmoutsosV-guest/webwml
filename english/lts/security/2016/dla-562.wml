<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>GOsa² is a combination of system-administrator and end-user web
interface, designed to handle LDAP based setups.</p>

<p>A code injection vulnerability in the Samba plugin code of GOsa has
been discovered. During Samba password changes it has been possible to
inject malicious Perl code.</p>

<p>If you upgrade to this fixed package revision, please note that Samba
password changes will stop working until the sambaHashHook parameter
in gosa.conf has been updated to accept base64 encoded password strings.</p>

<p>Please read the gosa.conf (5) man page after you have upgraded to this
package revision and adapt gosa.conf as described there.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.7.4-4.3~deb7u3.</p>

<p>We recommend that you upgrade your gosa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-562.data"
# $Id: $
