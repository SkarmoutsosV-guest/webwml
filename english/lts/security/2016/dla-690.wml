<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been discovered in the tar package that could allow
an attacker to overwrite arbitrary files through crafted files.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.26+dfsg-0.1+deb7u1.</p>

<p>We recommend that you upgrade your tar packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-690.data"
# $Id: $
