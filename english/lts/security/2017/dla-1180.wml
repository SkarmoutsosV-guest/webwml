<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tobias Schneider discovered that Spring-LDAP would allow authentication
with an arbitrary password when the username is correct, no additional
attributes are bound and when using LDAP BindAuthenticator with
DefaultTlsDirContextAuthenticationStrategy as the authentication
strategy and setting userSearch. This occurs because some LDAP vendors
require an explicit operation for the LDAP bind to take effect.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.3.1.RELEASE-4+deb7u1.</p>

<p>We recommend that you upgrade your libspring-ldap-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1180.data"
# $Id: $
