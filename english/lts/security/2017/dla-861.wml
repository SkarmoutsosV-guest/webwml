<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An exploitable buffer overflow vulnerability exists in the
LoadEncoding functionality of the R programming language. A
specially crafted R script can cause a buffer overflow
resulting in a memory corruption. An attacker can send a
malicious R script to trigger this vulnerability.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
2.15.1-4+deb7u1.</p>

<p>We recommend that you upgrade your r-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-861.data"
# $Id: $
