<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Secunia Research has discovered multiple vulnerabilities in libraw, a
raw image decoder library, which can be exploited to cause a Denial of
Service.
The issues contain divisions by zero, out-of-bounds read memory access,
heap-based buffer overflows and NULL pointer dereferences.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.16.0-9+deb8u4.</p>

<p>We recommend that you upgrade your libraw packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1734.data"
# $Id: $
