<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability was found in libapache2-mod-auth-openidc, the
OpenID Connect authentication module for the Apache HTTP server.</p>

<p>Insufficient validation of URLs leads to an Open Redirect
vulnerability. An attacker may trick a victim into providing credentials
for an OpenID provider by forwarding the request to an illegitimate
website.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.6.0-1+deb8u2.</p>

<p>We recommend that you upgrade your libapache2-mod-auth-openidc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1996.data"
# $Id: $
