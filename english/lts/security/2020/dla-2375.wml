<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were discovered in the modules of the InspIRCd IRC daemon, which could result in denial of service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20917">CVE-2019-20917</a>

    <p>mysql module before v3.3.0 contains a null pointer dereference when
    built against mariadb-connector-c. When combined with the sqlauth or
    sqloper modules this vulnerability can be used to remotely crash an
    InspIRCd server by any user able to connect to a server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25269">CVE-2020-25269</a>

    <p>The pgsql module contains a use after free vulnerability. When combined
    with the sqlauth or sqloper modules this vulnerability can be used to
    remotely crash an InspIRCd server by any user able to connect to a
    server.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.0.23-2+deb9u1.</p>

<p>We recommend that you upgrade your inspircd packages.</p>

<p>For the detailed security status of inspircd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/inspircd">https://security-tracker.debian.org/tracker/inspircd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2375.data"
# $Id: $
