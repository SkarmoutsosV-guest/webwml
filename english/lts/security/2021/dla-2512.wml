<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was found in hibernate-core. A SQL injection in the implementation
of the JPA Criteria API can permit unsanitized literals when a literal is
used in the SQL comments of the query. This flaw could allow an attacker to
access unauthorized information or possibly conduct further attacks.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.6.10.Final-6+deb9u1.</p>

<p>We recommend that you upgrade your libhibernate3-java packages.</p>

<p>For the detailed security status of libhibernate3-java please refer to
its security tracker page at:
<a  rel="nofollow" href="https://security-tracker.debian.org/tracker/libhibernate3-java">https://security-tracker.debian.org/tracker/libhibernate3-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a  rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2512.data"
# $Id: $
