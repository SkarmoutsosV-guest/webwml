<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0256">CVE-2020-0256</a>

    <p>In LoadPartitionTable of gpt.cc, there is a possible
    out of bounds write due to a missing bounds check. This
    could lead to local escalation of privilege with no
    additional execution privileges needed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0308">CVE-2021-0308</a>

    <p>In ReadLogicalParts of basicmbr.cc, there is a possible
    out of bounds write due to a missing bounds check. This
    could lead to local escalation of privilege with no
    additional execution privileges needed.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.0.1-1+deb9u1.</p>

<p>We recommend that you upgrade your gdisk packages.</p>

<p>For the detailed security status of gdisk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gdisk">https://security-tracker.debian.org/tracker/gdisk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2549.data"
# $Id: $
