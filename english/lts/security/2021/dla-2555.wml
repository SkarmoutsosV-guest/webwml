<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an insecure temporary file issue that could
have lead to disclosure of arbitrary local files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21290">CVE-2021-21290</a>

    <p>Netty is an open-source, asynchronous event-driven network application framework for rapid development of maintainable high performance protocol servers & clients. In Netty before version 4.1.59.Final there is a vulnerability on Unix-like systems involving an insecure temp file. [...]</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:4.1.7-2+deb9u3.</p>

<p>We recommend that you upgrade your netty packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2555.data"
# $Id: $
