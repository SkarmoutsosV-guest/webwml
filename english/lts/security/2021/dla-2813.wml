<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>CKEditor, an open source WYSIWYG HTML editor with rich content
support, which can be embedded into web pages, had two
vulnerabilites as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33829">CVE-2021-33829</a>

    <p>A cross-site scripting (XSS) vulnerability in the HTML Data
    Processor in CKEditor 4 allows remote attackers to inject
    executable JavaScript code through a crafted comment because
    --!> is mishandled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37695">CVE-2021-37695</a>

    <p>A potential vulnerability has been discovered in CKEditor 4
    Fake Objects package. The vulnerability allowed to inject
    malformed Fake Objects HTML, which could result in executing
    JavaScript code.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.5.7+dfsg-2+deb9u1.</p>

<p>We recommend that you upgrade your ckeditor packages.</p>

<p>For the detailed security status of ckeditor please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ckeditor">https://security-tracker.debian.org/tracker/ckeditor</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2813.data"
# $Id: $
