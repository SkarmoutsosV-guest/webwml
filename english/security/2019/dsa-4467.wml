<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>User <q>Arminius</q> discovered a vulnerability in Vim, an enhanced version of the
standard UNIX editor Vi (Vi IMproved). The <q>Common vulnerabilities and
exposures project</q> identifies the following problem:</p>

<p>Editors typically provide a way to embed editor configuration commands (aka
modelines) which are executed once a file is opened, while harmful commands
are filtered by a sandbox mechanism. It was discovered that the <q>source</q>
command (used to include and execute another file) was not filtered, allowing
shell command execution with a carefully crafted file opened in Vim.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 2:8.0.0197-4+deb9u2.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>For the detailed security status of vim please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/vim">\
https://security-tracker.debian.org/tracker/vim</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4467.data"
# $Id: $
