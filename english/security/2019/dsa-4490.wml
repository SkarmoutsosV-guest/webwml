<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Subversion, a version control
system. The Common Vulnerabilities and Exposures project identifies the
following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11782">CVE-2018-11782</a>

    <p>Ace Olszowka reported that the Subversion's svnserve server process
    may exit when a well-formed read-only request produces a particular
    answer, leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0203">CVE-2019-0203</a>

    <p>Tomas Bortoli reported that the Subversion's svnserve server process
    may exit when a client sends certain sequences of protocol commands.
    If the server is configured with anonymous access enabled this could
    lead to a remote unauthenticated denial of service.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1.9.5-1+deb9u4.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.10.4-1+deb10u1.</p>

<p>We recommend that you upgrade your subversion packages.</p>

<p>For the detailed security status of subversion please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/subversion">https://security-tracker.debian.org/tracker/subversion</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4490.data"
# $Id: $
