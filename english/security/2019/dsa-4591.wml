<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Stephan Zeisberg reported an out-of-bounds write vulnerability in the
_sasl_add_string() function in cyrus-sasl2, a library implementing the
Simple Authentication and Security Layer. A remote attacker can take
advantage of this issue to cause denial-of-service conditions for
applications using the library.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 2.1.27~101-g0780600+dfsg-3+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 2.1.27+dfsg-1+deb10u1.</p>

<p>We recommend that you upgrade your cyrus-sasl2 packages.</p>

<p>For the detailed security status of cyrus-sasl2 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cyrus-sasl2">https://security-tracker.debian.org/tracker/cyrus-sasl2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4591.data"
# $Id: $
