<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Bluez, the Linux Bluetooth
protocol stack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26558">CVE-2020-26558</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2021-0129">CVE-2021-0129</a></p>

    <p>It was discovered that Bluez does not properly check permissions
    during pairing operation, which could allow an attacker to
    impersonate the initiating device.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27153">CVE-2020-27153</a>

    <p>Jay LV discovered a double free flaw in the disconnect_cb() routine
    in the gattool. A remote attacker can take advantage of this flaw
    during service discovery for denial of service, or potentially,
    execution of arbitrary code.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 5.50-1.2~deb10u2.</p>

<p>We recommend that you upgrade your bluez packages.</p>

<p>For the detailed security status of bluez please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/bluez">https://security-tracker.debian.org/tracker/bluez</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4951.data"
# $Id: $
