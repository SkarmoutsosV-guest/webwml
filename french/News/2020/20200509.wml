#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.4</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la quatrième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis
à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apt-cacher-ng "Appel sécurisé imposé au serveur pour le déclenchement de tâches de maintenance [CVE-2020-5202] ; compression .zst permise pour les archives ; augmentation de la taille du tampon de ligne de décompression pour la lecture de fichier de configuration">
<correction backuppc "Passage du nom d'utilisateur à start-stop-daemon lors du rechargement empêchant les échecs de rechargement">
<correction base-files "Mise à jour pour cette version">
<correction brltty "Réduction de la sévérité des messages de connexion pour éviter de générer trop de messages lors de l'utilisation des nouvelles versions d'Orca">
<correction checkstyle "Correction d'un problème d'injection d'entité externe XML [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror "Mise à jour de la liste de miroirs incluse">
<correction clamav "Nouvelle version amont [CVE-2020-3123]">
<correction corosync "totemsrp : réduction du MTU pour éviter la création de paquets surdimensionnés">
<correction corosync-qdevice "Correction de démarrage du service">
<correction csync2 "Échec de la commande HELLO quand SSL est requis">
<correction cups "Correction de dépassement de tas [CVE-2020-3898] et de <q>la fonction « ippReadIO » peut effectuer une lecture hors limite d'un champ d'extension</q> [CVE-2019-8842]">
<correction dav4tbsync "Nouvelle version amont restaurant la compatibilité avec les dernières versions de Thunderbird">
<correction debian-edu-config "Ajout de fichiers de politique pour Firefox ESR et Thunderbird pour corriger la configuration de TLS/SSL">
<correction debian-installer "Mise à jour pour l'ABI du noyau 4.19.0-9">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-security-support "Nouvelle version amont stable ; mise à jour de l'état de plusieurs paquets ; utilisation de <q>runuser</q> à la place de <q>su</q>">
<correction distro-info-data "Ajout d'Ubuntu 20.10 et de la date probable de fin de prise en charge de Stretch">
<correction dojo "Correction de mauvaise utilisation d'expressions rationnelles [CVE-2019-10785]">
<correction dpdk "Nouvelle version amont stable">
<correction dtv-scan-tables "Nouvel instantané amont ; ajout de tous les multiplexes de DVB-T2 allemand et du satellite Eutelsat-5-West-A">
<correction eas4tbsync "Nouvelle version amont restaurant la compatibilité avec les dernières versions de Thunderbird">
<correction edk2 "Correctifs de sécurité [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api "Correction des mises à niveau de Stretch à Buster qui incluent Tomcat 8">
<correction fex "Correction d'un possible problème de sécurité dans fexsrv">
<correction filezilla "Correction d'une vulnérabilité de chemin de recherche non fiable [CVE-2019-5429]">
<correction frr "Correction de la capacité <q>next hop</q> étendue">
<correction fuse "Retrait des commandes udevadm obsolètes des scripts post-installation ; pas de retrait explicite de fuse.conf lors d'une purge">
<correction fuse3 "Retrait des commandes udevadm obsolètes des scripts post-installation ; pas de retrait explicite de fuse.conf lors d'une purge ; correction de fuite de mémoire dans fuse_session_new()">
<correction golang-github-prometheus-common "Extension de validité des certificats de test">
<correction gosa "Remplacement de <q>(un)serialize</q> par json_encode/json_decode pour réduire l'injection d'objet PHP [CVE-2019-14466]">
<correction hbci4java "Prise en charge de la directive européenne sur les services de paiement (PSD2)">
<correction hibiscus "Prise en charge de la directive européenne sur les services de paiement (PSD2)">
<correction iputils "Correction d'un problème dans lequel ping terminerait incorrectement avec un code d'erreur quand il y a encore des adresses non essayées disponibles dans la valeur de retour de l'appel de la bibliothèque getaddrinfo()">
<correction ircd-hybrid "Utilisation de dhparam.pem pour éviter un plantage au démarrage">
<correction jekyll "Utilisation de ruby-i18n 0.x et 1.x permise">
<correction jsp-api "Correction des mises à niveau de Stretch à Buster qui impliquent Tomcat 8">
<correction lemonldap-ng "Accès non désiré évité aux points de terminaison d'administration [CVE-2019-19791] ; correction du greffon GrantSession qui pourrait ne pas interdire la connexion lors de l'utilisation de l'authentification à deux facteurs ; correction de redirections arbitraires avec OIDC si redirect_uri n'est pas utilisé">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libreoffice "Correction de transition de diapositives d'OpenGL">
<correction libssh "Correction d'un potentiel problème de déni de service lors de la gestion de clés AES-CTR avec OpenSSL [CVE-2020-1730]">
<correction libvncserver "Correction de dépassement de tas [CVE-2019-15690]">
<correction linux "Nouvelle version amont stable">
<correction linux-latest "Mise à jour de l'ABI du noyau Linux 4.19.0-9">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction lwip "Correction de dépassement de tampon [CVE-2020-8597]">
<correction lxc-templates "Nouvelle version amont stable ; gestion des langues encodées uniquement en UTF-8">
<correction manila "Correction d'absence de droits d'accès [CVE-2020-9543]">
<correction megatools "Ajout de la prise en charge du nouveau format de liens de mega.nz">
<correction mew "Correction de vérification de validité de certificat SSL de serveur">
<correction mew-beta "Correction de vérification de validité de certificat SSL de serveur">
<correction mkvtoolnix "Reconstruction pour renforcer la dépendance de libmatroska6v5">
<correction ncbi-blast+ "Désactivation de la prise en charge de SSE4.2">
<correction node-anymatch "Retrait de dépendances non nécessaires">
<correction node-dot "Exécution de code évitée après pollution de prototype [CVE-2020-8141]">
<correction node-dot-prop "Correction de pollution de prototype [CVE-2020-8116]">
<correction node-knockout "Correction de protection avec les anciennes versions d'Internet Explorer [CVE-2019-14862]">
<correction node-mongodb "Rejet de _bsontypes non valables [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser "Correction de pollution de prototype [CVE-2020-7608]">
<correction npm "Correction d'accès à un chemin arbitraire [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers "Nouvelle version amont stable">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont stable">
<correction nvidia-settings-legacy-340xx "Nouvelle version amont">
<correction oar "Retour au comportement de Stretch pour la fonction Perl Storable::dclone corrigeant des problèmes de profondeur de récursion">
<correction opam "Mcss préféré à aspcud">
<correction openvswitch "Correction d'interruption de vswitchd quand un port est ajouté et que le contrôleur ne fonctionne pas">
<correction orocos-kdl "Correction de conversion de chaîne avec Python 3">
<correction owfs "Retrait de paquets Python 3 cassés">
<correction pango1.0 "Correction de plantage dans pango_fc_font_key_get_variations() quand la clé est nulle">
<correction pgcli "Ajout de dépendance manquante à python3-pkg-resources">
<correction php-horde-data "Correction d'une vulnérabilité d'exécution authentifiée de code distant [CVE-2020-8518]">
<correction php-horde-form "Correction d'une vulnérabilité d'exécution authentifiée de code distant [CVE-2020-8866]">
<correction php-horde-trean "Correction d'une vulnérabilité d'exécution authentifiée de code distant [CVE-2020-8865]">
<correction postfix "Nouvelle version amont stable ; correction de panique avec la configuration de Postfix avec plusieurs <q>Milter</q> pendant MAIL FROM ; correction de modification d'exécution de d/init.d de sorte qu'il fonctionne à nouveau avec plusieurs instances">
<correction proftpd-dfsg "Correction d'un problème d'accès mémoire dans le code de clavier interactif dans mod_sftp ; gestion correcte des messages DEBUG, IGNORE, DISCONNECT et UNIMPLEMENTED dans le mode clavier interractif">
<correction puma "Correction d'un problème de déni de service [CVE-2019-16770]">
<correction purple-discord "Correction de plantages dans ssl_nss_read">
<correction python-oslo.utils "Correction de fuite d'informations sensibles au moyen de journaux de mistral [CVE-2019-3866]">
<correction rails "Correction d'une potentielle vulnérabilité de script intersite au moyen de l'assistant d'échappement de Javascript [CVE-2020-5267]">
<correction rake "Correction d'un vulnérabilité d'injection de commande [CVE-2020-8130]">
<correction raspi3-firmware "Correction d'erreur de noms dtb dans z50-raspi-firmware ; correction de démarrage sur les Raspberry Pi familles 1 et 0">
<correction resource-agents "Correction de <q>ethmonitor ne liste pas les interfaces sans adresses IP assignées</q> ; retrait du correctif de xen-toolstack qui n'est plus requis ; correction d'une utilisation non standard dans l'agent ZFS">
<correction rootskel "Désactivation de la prise en charge de plusieurs consoles lors de l'utilisation de la préconfiguration">
<correction ruby-i18n "Correction de génération de gemspec">
<correction rubygems-integration "Avertissements d'obsolescence évités quand les utilisateurs installent une nouvelle version de Rubygems au moyen de <q>gem update --system</q>">
<correction schleuder "Amélioration du correctif pour gérer les erreurs d'encodage introduites dans la version précédente ; passage de l'encodage par défaut à UTF-8 ; gestion par x-add-key de courriels des clés attachées encodées au format <q>Quoted-Printable</q> ; correction de x-attach-listkey avec des courriels créés par Thunderbird qui comprennent des en-têtes protégés">
<correction scilab "Correction du chargement de la bibliothèque avec OpenJDK 11.0.7">
<correction serverspec-runner "Prise en charge de Ruby 2.5">
<correction softflowd "Correction d’agrégation de flux cassée qui pourrait provoquer un débordement de table de flux et une utilisation de processeur à 100 %">
<correction speech-dispatcher "Correction de la latence par défaut de pulseaudio qui déclenche une sortie <q>scratchy</q>">
<correction spl-linux "Correction de blocage">
<correction sssd "Correction d'attente active de sssd_be quand la connexion de LDAP est intermittente">
<correction systemd "Nouvelle résolution de callback/userdata à la place d'une mise en cache lors d'une autorisation au moyen de PolicyKit [CVE-2020-1712] ; installation de 60-block.rules dans udev-udeb et initramfs-tools">
<correction taglib "Correction de problèmes de corruption avec les fichiers OGG">
<correction tbsync "Nouvelle version amont restaurant la compatibilité avec les dernières versions de Thunderbird">
<correction timeshift "Correction d'utilisation de répertoire temporaire prévisible [CVE-2020-10174]">
<correction tinyproxy "Configuration de PIDDIR seulement si PIDFILE est une chaîne de longueur différente de zéro">
<correction tzdata "Nouvelle version amont stable">
<correction uim "Désinscription des modules qui ne sont pas installés, correction d'une régression dans l'envoi précédent">
<correction user-mode-linux "Correction d'échec de construction avec les noyaux stables actuels">
<correction vite "Correction de plantage quand il y a plus de 32 éléments">
<correction waagent "Nouvelle version amont ; prise en charge de la co-installation avec cloud-init">
<correction websocket-api "Correction des mises à niveau de Stretch à Buster qui incluent Tomcat 8">
<correction wpa "Pas d'essai de détection d'erreur PSK durant la recomposition de PTK ; vérification de la prise en charge de FT lors de la sélection des suites FT ; correction d'un problème de randomisation de MAC avec certaines cartes">
<correction xdg-utils "xdg-open : correction de vérification de pcmanfm et de gestion de répertoires avec des espaces dans leur nom ; xdg-screensaver : nettoyage du nom de fenêtre avant de l'envoyer sur D-Bus ; xdg-mime : création du répertoire config s'il n'existe pas encore">
<correction xtrlock "Correction du blocage de (certains) périphériques multipoints lors du verrouillage [CVE-2016-10894]">
<correction zfs-linux "Correction de possibles problèmes de blocage">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction getlive "Cassé par des modifications de Hotmail">
<correction gplaycli "Cassé par des modifications de l'API de Google">
<correction kerneloops "Service amont plus disponible">
<correction lambda-align2 "[arm64 armel armhf i386 mips64el ppc64el s390x] Cassé sur les architectures non amd64">
<correction libmicrodns "Problèmes de sécurité">
<correction libperlspeak-perl "Problèmes de sécurité ; non maintenu">
<correction quotecolors "Incompatible avec les dernières versions de Thunderbird">
<correction torbirdy "Incompatible avec les dernières versions de Thunderbird">
<correction ugene "Non libre ; échec de construction">
<correction yahoo2mbox "Cassé depuis plusieurs années">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>


<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
