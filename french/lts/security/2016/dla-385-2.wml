#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Avec l'envoi précédent du paquet isc-dhcp pour Debian Squeeze LTS
deux problèmes ont été introduits dans LTS qui sont résolus par cet envoi.</p>

<ul>

<li>(1)

<p>Le <a href="https://security-tracker.debian.org/tracker/CVE-2015-8605">CVE-2015-8605</a>
avait été seulement résolu pour la variante LDAP du paquet du serveur DHCP
construit à partir du paquet source isc-dhcp. Dans la
version 4.1.1-P1-15+squeeze10, maintenant toutes les variantes du serveur
DHCP (aussi bien LDAP et non LDAP) incluent le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2015-8605">CVE-2015-8605</a>.
Merci à Ben Hutchings pour avoir repéré cette erreur.</p></li>

<li>(2)

<p>La construction du binaire amd64 de la version d'isc-dhcp précédemment
envoyée (4.1.1-P1-15+squeeze9) était défectueuse et recherchait le fichier
de configuration dhcpd.conf à un mauvais emplacement [1,2,3]. Ce défaut
dans la construction amd64 a été provoqué par un système de construction
pas squeeze-lts à 100 % du côté du responsable du paquet. La construction
amd64 de la version 4.1.1-P1-15+squeeze10 a été refaite dans un
environnement de construction entièrement nouveau et ne montre plus les
symptômes signalés.</p>

<p>Je m'excuse profondément pour les inconvénients rencontrés à tous ceux
qui ont subi ce problème.</p></li>

</ul>

<p>[1] <a href="https://bugs.debian.org/811097">https://bugs.debian.org/811097</a><br>
[2] <a href="https://bugs.debian.org/811397">https://bugs.debian.org/811397</a><br>
[3] <a href="https://bugs.debian.org/811402">https://bugs.debian.org/811402</a></p>

<p>Pour Debian 6 Squeeze, ces problèmes ont été corrigés dans la
version 4.1.1-P1-15+squeeze10 d'isc-dhcp.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-385-2.data"
# $Id: $
