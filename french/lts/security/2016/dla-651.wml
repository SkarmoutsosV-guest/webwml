#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Divers problèmes de sécurité ont été découverts et corrigés dans
graphicsmagick dans Debian 7 <q>Wheezy</q> LTS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7446">CVE-2016-7446</a>

<p>Problème de dépassement de tas dans le rendu de fichiers MVG/SVG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7447">CVE-2016-7447</a>

<p>Dépassement de tas de la fonction EscapeParenthesis()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7449">CVE-2016-7449</a>

<p>Problèmes liés à TIFF dus à l'utilisation de strlcpy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7800">CVE-2016-7800</a>

<p>Correction d'un dépassement d'entier par le bas qui mène à un
dépassement de tas lors de l'analyse de blocs de type 8BIM.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.3.16-1.1+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-651.data"
# $Id: $
