#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de script intersite réfléchi (XSS) ont été
découvertes dans SPIP, un moteur de publication pour site web écrit en PHP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9997">CVE-2016-9997</a>

<p>Le paramètre <q>id</q> pour l'action puce_statut n'est pas vérifié
correctement. Un attaquant pourrait injecter du code HTML arbitraire en
piégeant un utilisateur authentifié de SPIP dans l'ouverture d'une URL
contrefaite pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9998">CVE-2016-9998</a>

<p>Le paramètre <q>plugin</q> pour l'action info_plugin n'est pas vérifié
correctement. Un attaquant pourrait injecter du code HTML arbitraire en
piégeant un utilisateur authentifié de SPIP dans l'ouverture d'une URL
contrefaite pour l'occasion.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.1.17-1+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spip.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-760.data"
# $Id: $
