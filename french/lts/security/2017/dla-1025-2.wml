#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de sécurité publiée sous DLA-1025-1 introduisait une
régression dans bind9.</p>

<p>Le correctif pour <a href="https://security-tracker.debian.org/tracker/CVE-2017-3142">CVE-2017-3142</a>
cassait la vérification de séquences de messages TCP signés TSIG où tous les
messages ne contenaient pas d’enregistrements TSIG. Cela est conforme à la
spécification et peut être utilisé dans des réponses AXFR et IXFR.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:9.8.4.dfsg.P1-6+nmu2+deb7u18.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1025-2.data"
# $Id: $
