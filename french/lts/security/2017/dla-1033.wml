#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il existait une vulnérabilité de déni de service dans memcached, un
système de mise en cache d’objets mémoire de haute performance.</p>

<p>La fonction try_read_command permettait à des attaquants distants de
provoquer un déni de service à l'aide d'une requête pour ajouter ou régler une
clef, qui réalise une comparaison entre un entier signé ou non signé qui
déclenchait une lecture hors limites de tampon basé sur le tas.</p>

<p>Cette vulnérabilité existait à cause d’un correctif incomplet pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8705">CVE-2016-8705</a>.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 1.4.13-0.2+deb7u3 de memcached.</p>

<p>Nous vous recommandons de mettre à jour vos paquets memcached.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1033.data"
# $Id: $
