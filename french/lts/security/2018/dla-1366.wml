#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans wordpress, un outil de blog.
Le projet « Common Vulnerabilities and Exposures » (CVE) identifie les problèmes
suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10100">CVE-2018-10100</a>

<p>La redirection d’URL pour la page de connexion n’était pas validée ou
vérifiée si l’utilisation d’HTTPS était forcée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10102">CVE-2018-10102</a>

<p>La chaîne de version n’était pas protégée dans la fonction get_the_generator,
et pourrait conduire à un script intersite (XSS) dans une balise « generator ».</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.1+dfsg-1~deb7u21.</p>
<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1366.data"
# $Id: $
