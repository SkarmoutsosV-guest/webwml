#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités d’exécution de code à distance dans php-horde-image, la
bibliothèque de traitement d'images pour la suite de travail collaboratif Horde
<url "https://www.horde.org/">.</p>

<ul>

 <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9774">CVE-2017-9774</a>
<p>Une vulnérabilité d’exécution de code à distance (RCE) qui était exploitable par
un utilisateur connecté envoyant une requête GET HTTP contrefaite et malveillante
à divers dorsaux d’image.</p>

<p>Remarquez que le correctif appliqué en amont possède une régression en ce
qu’il ignore l’option <q>force aspect ratio</q>. Consulter
<a href="https://github.com/horde/Image/pull/1">https://github.com/horde/Image/pull/1</a>.</p></li>

 <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14650">CVE-2017-14650</a>
<p>Une autre RCE qui était exploitable par un utilisateur connecté envoyant une
requête GET contrefaite et malveillante au dorsal <q>im</q> d’image.</p></li>

</ul>

<p> Pour Debian 8 <q>Jessie</q>, ces problèmes ont été résolus dans la
version 2.1.0-4+deb8u1 de php-horde-image.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php-horde-image.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1395.data"
# $Id: $
