#use wml::debian::translation-check translation="945b5eb7448847216cc28fdb7b07df40fa0ef7a1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18249">CVE-2017-18249</a>

<p>Une situation de compétition a été découverte dans l’allocation d’espace
disque pour F2FS. Un utilisateur ayant accès à un volume F2FS pourrait utiliser
cela pour provoquer un déni de service ou un autre impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1128">CVE-2018-1128</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-1129">CVE-2018-1129</a>

<p>Le protocole d’authentification cephx utilisé par Ceph était vulnérable à des
attaques par rejeu, et calculait les signatures incorrectement. Ces
vulnérabilités dans le serveur nécessitaient des modifications pour
l’authentification qui étaient incompatibles avec les clients existants. Le
code du client de noyau a été mis à jour pour être compatible avec le serveur
corrigé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a> (SSB)

<p>Plusieurs chercheurs ont découvert que SSB (Speculative Store Bypass), une
fonction implémentée dans beaucoup de processeurs, pourrait être utilisée pour
lire des informations sensibles d’autre contexte. En particulier, du code dans
un autre bac à sable logiciel pourrait lire des informations sensibles en dehors
du bac à sable. Ce problème est aussi connu comme Spectre variante 4.</p>

<p>Cette mise à jour ajoute une autre mitigation pour ce problème dans
l’implémentation d’eBPF (Extended Berkeley Packet Filter).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5391">CVE-2018-5391</a> (FragmentSmack)

<p>Juha-Matti Tilli a découvert un défaut dans la manière dont le noyau Linux
gère le réassemblage de paquets fragmentés IPv4 et IPv6. Un attaquant distant
peut exploiter ce défaut pour déclencher des algorithmes de réassemblage de
fragments, coûteux en temps et calcul, en envoyant des paquets contrefaits pour
l'occasion, aboutissant à un déni de service.</p>

<p>Cela était précédemment mitigé en réduisant les limites par défaut
d’utilisation de mémoire pour des paquets fragmentés incomplets. Cette mise
à jour remplace cette mitigation avec un correctif plus complet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5848">CVE-2018-5848</a>

<p>Le pilote wifi wil6210 ne validait pas correctement la longueur des requêtes
d’analyse et connexion, conduisant à un dépassement possible de tampon. Dans les
systèmes utilisant ce pilote, un utilisateur local ayant la capacité
CAP_NET_ADMIN pourrait utiliser cela pour un déni de service (corruption de
mémoire ou plantage) ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12896">CVE-2018-12896</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13053">CVE-2018-13053</a>

<p>Team OWL337 a signalé un dépassement possible d'entier dans l’implémentation
POSIX de timer. Cela pourrait avoir un impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13096">CVE-2018-13096</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13097">CVE-2018-13097</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13100">CVE-2018-13100</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14614">CVE-2018-14614</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14616">CVE-2018-14616</a>

<p>Wen Xu de SSLab à Gatech a signalé que des volumes F2FS contrefaits
pourraient déclencher un plantage (bogue, Oops ou division par zéro) ou un accès
mémoire hors limites. Un attaquant capable de monter un tel volume pourrait
utiliser cela pour provoquer un déni de service ou éventuellement pour une
augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13406">CVE-2018-13406</a>

<p>Dr Silvio Cesare d’InfoSect a signalé un dépassement potentiel d'entier dans
le pilote uvesafb. Un utilisateur ayant la permission d’accéder à un tel
périphérique pourrait être capable d’utiliser cela pour un déni de service ou
une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14610">CVE-2018-14610</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14611">CVE-2018-14611</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14612">CVE-2018-14612</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-14613">CVE-2018-14613</a>

<p>Wen Xu de SSLab à Gatech a signalé que des volumes Btrfs contrefaits
pourraient déclencher un plantage (Oops) ou un accès mémoire hors limites. Un
attaquant capable de monter un tel volume pourrait utiliser cela pour provoquer
un déni de service ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15471">CVE-2018-15471</a> (XSA-270)

<p>Felix Wilhelm de Google Project Zero a découvert un défaut dans le traitement
du hachage du module de noyau Linux xen-netback. Une interface malveillante ou
boguée peut amener le dorsal (habituellement nécessitant des droits) d’accéder
hors limites à la mémoire, aboutissant éventuellement à une augmentation de
droits, un déni de service ou une fuite d'informations.</p>

<p>https://xenbits.xen.org/xsa/advisory-270.html</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16862">CVE-2018-16862</a>

<p>Vasily Averin et Pavel Tikhomirov de l’équipe Virtuozzo Kernel ont découvert
que la fonction de gestion mémoire cleancache n’annulait pas les données
en cache des fichiers effacés. Sur des invités Xen utilisant le pilote tmem, des
utilisateurs locaux pourraient éventuellement lire des données d’autres fichiers
d’utilisateur supprimés s’ils pouvaient créer de nouveaux fichiers dans le même
volume.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17972">CVE-2018-17972</a>

<p>Jann Horn a signalé que les fichiers /proc/*/stack dans procfs divulguaient
des données sensibles du noyau. Ces fichiers sont maintenant seulement lisibles
par les utilisateurs ayant la capacité CAP_SYS_ADMIN (habituellement le
superutilisateur).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18281">CVE-2018-18281</a>

<p>Jann Horn a signalé une situation de compétition dans le gestionnaire de
mémoire virtuelle. Cela peut aboutir à un processus ayant brièvement accès à la
mémoire après qu’elle ait été libérée et réallouée. Un utilisateur local pourrait
éventuellement exploiter cela pour un déni de service (corruption de mémoire) ou
pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18690">CVE-2018-18690</a>

<p>Kanda Motohiro a signalé que XFS ne gérait pas correctement des écritures
xattr (attribut étendu) qui nécessitaient un changement de format de disque de
xattr. Un utilisateur ayant accès à un volume XFS pourrait utiliser cela pour un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18710">CVE-2018-18710</a>

<p>Le pilote de CD-ROM ne validait pas correctement le paramètre ioctl
CDROM_SELECT_DISC. Un utilisateur ayant accès à un tel périphérique pourrait
utiliser cela pour lire des informations sensibles du noyau ou pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19407">CVE-2018-19407</a>

<p>Wei Wu a signalé un plantage potentiel (Oops) dans l’implémentation de KVM
pour les processeurs x86. Un utilisateur ayant accès à /dev/kvm pourrait
utiliser cela pour un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.144-3.1~deb8u1. Cette version inclut aussi des correctifs pour
les bogues de Debian n° 890034, n° 896911, n° 907581, n° 915229 et n° 915231,
et d’autres correctifs inclus dans les mises à jour amont de stable.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1715.data"
# $Id: $
