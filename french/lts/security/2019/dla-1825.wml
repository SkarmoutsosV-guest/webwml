#use wml::debian::translation-check translation="17680830bc0ef206b01bdb9b607c7412d22a9255" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un oracle de déchiffrement basé sur la réponse a été découvert dans kdepim
qui fournit le client de courriel KMail.</p>

<p>Un attaquant en possession de courriels chiffrés S/MIME ou PGP peut les
envelopper comme sous-parties dans un courriel multi-partie contrefait. La ou
les parties chiffrées peuvent être plus tard être cachées en utilisant des
caractères de nouvelle ligne HTML/CSS ou ASCII. Ce courriel multi-partie modifié
peut être réexpédié par l’attaquant au destinataire prévu. Si le destinataire
répond à ce courriel (d’apparence anodine), il divulgue involontairement en
retour à l’attaquant le texte en clair des parties chiffrées du message.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 4:4.14.1-1+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets kdepim.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1825.data"
# $Id: $
