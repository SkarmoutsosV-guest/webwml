#use wml::debian::translation-check translation="7f681b3f47dd0796e3a9f61a39732e6fb10ba33f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été signalés à propos de yubikey-val.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10184">CVE-2020-10184</a>

<p>Le terminal de vérification dans le serveur de vérification d’YubiKey avant
la version 2.40 ne vérifiait pas la longueur des requêtes SQL. Cela permettait
à des attaquants distants de provoquer un déni de service, autrement dit, une
injection SQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10185">CVE-2020-10185</a>

<p>Le terminal de vérification dans le serveur de vérification d’YubiKey avant
la version 2.40 permettait à des attaquants distants de rejouer un OTP.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.27-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets yubikey-val.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2141.data"
# $Id: $
