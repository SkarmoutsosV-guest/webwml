#use wml::debian::translation-check translation="3a266cc03b726ac00e9a1ee8c08ca7d5ddf8cd24" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs CVE ont été découverts dans le paquet src:wordpress.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11026">CVE-2020-11026</a>

<p>Les fichiers avec un nom contrefait pour l'occasion lorsque téléchargés dans
la section Media pouvaient conduire à une exécution de script lors de l’accession
à un fichier. Cela nécessitait un utilisateur authentifié avec des droits de
téléchargement de fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11027">CVE-2020-11027</a>

<p>Le lien de réinitialisation du mot de passe envoyé par courriel à un
utilisateur n’expirait pas après un changement de mot de passe d’utilisateur.
Un accès au compte de courriel de l’utilisateur serait nécessaire au tiers
malveillant pour une exécution réussie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11028">CVE-2020-11028</a>

<p>Certaines publications privées, précédemment publiques, pouvaient aboutir
à une diffusion non authentifiée sous un ensemble de conditions particulières.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11029">CVE-2020-11029</a>

<p>Une vulnérabilité dans la méthode stats() de class-wp-object-cache.php
pouvait être exploitée pour exécuter des attaques par script intersite (XSS).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.1.30+dfsg-0+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2208.data"
# $Id: $
