#use wml::debian::translation-check translation="bb3f804000185056dc659c88709d05facf94aa3f" maintainer="Jean-Pierre Giraud"
<define-tag description>Fin de vie de la prise en charge à long terme de Debian 8</define-tag>
<define-tag moreinfo>
<p>
L'équipe du suivi à long terme (LTS) de Debian annonce par ce message
que la prise en charge de Debian 8 <q>Jessie</q> a atteint sa fin de vie
le 30 juin 2020, cinq ans après sa publication initiale le 26 avril 2015.
</p>
<p>
Debian ne fournira plus désormais de mises à jour de sécurité pour
Debian 8. Un sous-ensemble de paquets de <q>Jessie</q> sera pris en charge
par des intervenants extérieurs. Vous trouverez des informations détaillées
sur la page <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.
</p>
<p>
L'équipe LTS préparera la transition vers Debian 9 <q>Stretch</q> qui
est la distribution <q>oldstable</q> actuelle. L'équipe LTS a pris le
relais de l'équipe en charge de la sécurité pour le suivi le
6 juillet 2020, tandis que la dernière mise à jour de <q>Stretch</q> sera
publiée le 18 juillet 2020.
</p>
<p>
Debian 9 recevra aussi un suivi à long terme de cinq ans à compter de sa
publication initiale et qui prendra fin le 30 juin 2022. Les architectures
prises en charges comprennent toujours amd64, i386, armel et armhf. De plus,
nous sommes heureux d'annoncer que, pour la première fois, la prise en
charge sera élargie pour inclure l'architecture arm64.
</p>
<p>
Pour plus d'informations sur l'utilisation de <q>Stretch</q> LTS et la
mise à niveau à partir de <q>Jessie</q> LTS, veuillez vous référer à la
page du wiki <a href="https://wiki.debian.org/fr/LTS/Using">LTS/Using</a>.
</p>
<p>
Debian et l'équipe LTS aimeraient remercier tous ceux, utilisateurs
contributeurs, développeurs et parrains, qui rendent possible le
prolongement de la vie des anciennes distributions <q>stable</q> et qui ont
fait de LTS un succès.
</p>
<p>
Si vous dépendez de Debian LTS, vous pouvez envisager de
<a href="https://wiki.debian.org/fr/LTS/Development">rejoindre l'équipe</a>,
en fournissant des correctifs, en réalisant des tests ou en
<a href="https://wiki.debian.org/fr/LTS/Funding">finançant l'initiative</a>.
</p>
<p>
Vous trouverez plus d'informations sur le suivi à long terme (LTS) de
Debian sur la <a href="https://wiki.debian.org/LTS/">page LTS du wiki</a>.
</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2272.data"
# $Id: $
