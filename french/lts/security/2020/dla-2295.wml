#use wml::debian::translation-check translation="5408f20ddd87b2b7613e417ac92c3c534716da84" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans curl, un outil en ligne de commande
pour transférer des données avec une syntaxe d’URL.</p>

<p>Lors de l’utilisation de -J (--remote-header-name) et -i (--include) dans la
même ligne de commande, un serveur malveillant pourrait forcer curl à écraser
le contenu de fichiers locaux avec les en-têtes d’HTTP entrant.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 7.52.1-5+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2295.data"
# $Id: $
