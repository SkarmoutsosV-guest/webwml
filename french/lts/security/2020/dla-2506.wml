#use wml::debian::translation-check translation="efc5297532a3896b815f5b52cfadf54c2c683a5c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert qu’Awstats, un analyseur de journaux de serveur web, était
vulnérable à des attaques de traversée de répertoires. Un attaquant distant non
authentifié pourrait exploiter cela pour réaliser une exécution de code
arbitraire. Le précédent correctif ne remédiait pas complètement au problème
quand le fichier /etc/awstats/awstats.conf n’était pas présent.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 7.6+dfsg-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets awstats.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de awstats, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/awstats">https://security-tracker.debian.org/tracker/awstats</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2506.data"
# $Id: $
