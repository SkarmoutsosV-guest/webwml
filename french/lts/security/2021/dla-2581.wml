#use wml::debian::translation-check translation="8e2c1511a2dcc4f6d5cfe5404d5cd17002a7bdcb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans la façon dont p2p/p2p_pd.c dans
wpa_supplicant avant la version 2.10 traite les requêtes P2P (Wi-Fi Direct) de
recherche de disponibilité. Elle pourrait aboutir à un déni de service ou
à un autre impact (exécution potentielle de code arbitraire) si un attaquant
est à portée d’émetteur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2:2.4-1+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2581.data"
# $Id: $
