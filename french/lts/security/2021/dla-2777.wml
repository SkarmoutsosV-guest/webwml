#use wml::debian::translation-check translation="a8750169a18aa9e9b4ef99592969611190f0e458" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été trouvés dans TIFF, un format largement
utilisé pour enregistrer des données d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19131">CVE-2020-19131</a>

<p>Un dépassement de tampon dans LibTiff permet à des attaquants de provoquer
un déni de service à l’aide de la fonction <q>invertImage()</q> dans le
composant <q>tiffcrop</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19144">CVE-2020-19144</a>

<p>Un dépassement de tampon dans LibTiff permet à des attaquants de provoquer
un déni de service à l’aide de la fonction <q>_TIFFmemcpy</q> dans le
composant <q>tif_unix.c</q>.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 4.0.8-2+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tiff,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tiff">\
https://security-tracker.debian.org/tracker/tiff</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2777.data"
# $Id: $
