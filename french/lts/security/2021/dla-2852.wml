#use wml::debian::translation-check translation="e73f7bf3aaf6798bb3840f386182bda7f568bdea" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Apache
Log4j2, une infrastructure de journalisation répandue pour Java, qui
pourraient mener à un déni de service ou à la divulgation d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9488">CVE-2020-9488</a>

<p>Une validation incorrecte de certificat avec erreur d'hôte dans le flux
(« appender ») SMTP de log4j d'Apache. Cela pourrait permettre
l'interception d'une connexion SMTPS par une attaque de type homme du
milieu qui pourrait aboutir à une divulgation de n'importe quel message de
journal envoyé au moyen de cet « appender ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45105">CVE-2021-45105</a>

<p>Apache Log4j2 ne protégeait pas contre une récursion non contrôlée à
partir de recherches auto-référencées. Cela permet à un attaquant ayant le
contrôle sur des données d'entrée de carte de contextes de thread (MDC) de
provoquer une déni de service lors de l'interprétation d'une chaîne
contrefaite.</p></li>

</ul>

<p>Pour Debian 9 Stretch, ces problèmes ont été corrigés dans la version
2.12.3-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache-log4j2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache-log4j2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2852.data"
# $Id: $
