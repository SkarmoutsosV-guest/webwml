#use wml::debian::translation-check translation="70f6c779d9ffe72b126eadb87cb48e43c8dec4db" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que dans libphp-adodb, une bibliothèque PHP de couche
d'abstraction de base de données, un attaquant peut injecter des valeurs
dans la chaîne de connexion de PostgreSQL en contournant
adodb_addslashes(). La fonction peut être contournée dans phppgadmin, par
exemple, en entourant le nom d'utilisateur par des guillemets et en le
soumettant avec d'autres paramètres injectés à l'intérieur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
5.20.9-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libphp-adodb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libphp-adodb,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libphp-adodb">\
https://security-tracker.debian.org/tracker/libphp-adodb</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2912.data"
# $Id: $
