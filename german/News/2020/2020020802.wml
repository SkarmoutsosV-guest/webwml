<define-tag pagetitle>Debian 9 aktualisiert: 9.12 veröffentlicht</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="2913230d58de12a2d0daeab2fd1f532a65fa5c2a" maintainer="Erik Pfannenstein"

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die zwölfte Aktualisierung seiner 
Oldstable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Oldstable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können.
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser
Aktualisierungen sind in dieser Revision enthalten.
</p>


<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Oldstable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction base-files "Aktualisierung für die Zwischenveröffentlichung">
<correction cargo "Neue Version der Originalautoren zwecks Unterstützung von Firefox-ESR-Rückportierungen; Bootstrap für armhf überarbeitet">
<correction clamav "Neue Veröffentlichung der Originalautoren; Problem mit Dienstblockade behoben [CVE-2019-15961]; ScanOnAccess-Option entfernt und mit clamonacc ersetzt">
<correction cups "Validierung der Standardsprache in ippSetValuetag überarbeitet [CVE-2019-2228]">
<correction debian-installer "Neukompilierung gegen oldstable-proposed-updates; gfxpayload=keep auch in den Untermenüs anwenden, um bei den Netboot-Abbildern, die via EFI gestartet wurden, die Unleserlichkeit der Schrift auf hochauflösenden Bildschirmen zu beheben; USE_UDEBS_FROM-Vorgabe von Unstable auf Stretch geändert, um Nutzern zu helfen, die lokal kompilieren">
<correction debian-installer-netboot-images "Neukompilierung gegen stretch-proposed-updates">
<correction debian-security-support "Status der Sicherheitsunterstützung für mehrere Pakete aktualisiert">
<correction dehydrated "Neue Veröffentlichung der Originalautoren; standardmäßig ACMEv2-API benutzen">
<correction dispmua "Neue Veröffentlichung der Originalautoren, die kompatibel mit Thunderbird 68 ist">
<correction dpdk "Neue stabile Veröffentlichung der Originalautoren; vhost-Regression behoben, die durch die Korrektur für CVE-2019-14818 entstanden ist">
<correction fence-agents "Unvollständige Entfernung von fence_amt_ws behoben">
<correction fig2dev "Fig-v2-Textzeichenketten, die mit mehreren ^A enden, erlauben [CVE-2019-19555]">
<correction flightcrew "Sicherheitskorrekturen [CVE-2019-13032 CVE-2019-13241]">
<correction freetype "Correctly handle deltas in TrueType GX fonts, fixing rendering of variable hinted fonts in Chromium and Firefox">
<correction glib2.0 "Ensure libdbus clients can authenticate with a GDBusServer like the one in ibus">
<correction gnustep-base "Fix UDP amplification vulnerability">
<correction italc "Sicherheitskorrekturen [CVE-2018-15126 CVE-2018-15127 CVE-2018-20019 CVE-2018-20020 CVE-2018-20021 CVE-2018-20022 CVE-2018-20023 CVE-2018-20024 CVE-2018-20748 CVE-2018-20749 CVE-2018-20750 CVE-2018-6307 CVE-2018-7225 CVE-2019-15681]">
<correction libdate-holidays-de-perl "Internationalen Tag der Kinder (20. September) in Thüringen ab 2019 als Feiertag markieren">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libidn "Anfälligkeit für Dienstblockaden in der Punycode-Handhabung behoben [CVE-2017-14062]">
<correction libjaxen-java "Kompilierungsfehlschlag durch Erlauben von Testfehlschlägen behoben">
<correction libofx "Problem mit Nullzeiger-Dereferenzierung behoben [CVE-2019-9656]">
<correction libole-storage-lite-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libparse-win32registry-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libperl4-corelibs-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libpst "Erkennung von get_current_dir_name und Rückgabebeschneidung überarbeitet">
<correction libsixel "Mehrere Sicherheitsprobleme behoben [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libsolv "Heap-Pufferüberlauf behoben [CVE-2019-20387]">
<correction libtest-mocktime-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libtimedate-perl "Interpretation der Jahre ab 2020 korrigiert">
<correction libvncserver "RFBserver: keinen Stack-Speicher an die Gegenstelle durchsickern lassen [CVE-2019-15681]; Einfrieren während des Abbauens von Verbindungen und einen Speicherzugriffsfehler auf Multi-Thread-VNC-Servern behoben; Probleme beim Verbinden mit VMWare-Servern behoben; Absturz von x11vnc, wenn vncviewer eine Verbindung aufbaut, behoben">
<correction libxslt "Ins Leere deutenden Zeiger in xsltCopyText behoben [CVE-2019-18197]">
<correction limnoria "Informationsoffenlegung in die Ferne und mögliche Fern-Codeausführung aus der Ferne im Math-Plugin behoben [CVE-2019-19010]">
<correction linux "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-latest "Aktualisierung für Linux-Kernel-ABI 4.9.0-12">
<correction llvm-toolchain-7 "Den Gold-Linker von s390x abschalten; für Bootstrap -fno-addrsig benutzen, mit Stretch's binutils funktioniert es auf  mips64el nicht">
<correction mariadb-10.1 "Neue stabile Version der Originalautoren [CVE-2019-2974 CVE-2020-2574]">
<correction monit "Positionsunabhängigen CSRF-Cookie-Wert implementiert">
<correction node-fstream "Verknüpfung überschreiben, wenn sie einer Datei im Weg ist [CVE-2019-13173]">
<correction node-mixin-deep "Prototype-Pollution behoben [CVE-2018-3719 CVE-2019-10746]">
<correction nodejs-mozilla "Neues Paket, um Firefox-ESR-Rückportierungen zu unterstützen">
<correction nvidia-graphics-drivers-legacy-340xx "Neue stabile Veröffentlichung der Originalautoren">
<correction nyancat "Neukompilierung in sauberer Umgebung, um die systemd-Unit für nyancat-server hinzuzufügen">
<correction openjpeg2 "Heap-Überlauf [CVE-2018-21010], Ganzzahlüberlauf [CVE-2018-20847] und Teilung durch null [CVE-2016-9112] behoben">
<correction perl "Interpretation der Jahre ab 2020 korrigiert">
<correction php-horde "Stored-Cross-Site-Scripting-Problem im Horde Cloud Block behoben [CVE-2019-12095]">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren; Problemumgehung für unzureichende TCP-Loopback-Leistung eingebaut">
<correction postgresql-9.6 "Neue Veröffentlichung der Originalautoren">
<correction proftpd-dfsg "Nullzeiger-Dereferenzierung in CRL-Prüfungen behoben [CVE-2019-19269]">
<correction pykaraoke "Pfad zu den Fonts korrigiert">
<correction python-acme "Wechsel zum POST-as-GET-Protokoll">
<correction python-cryptography "Testsuite-Fehlschläge, wenn gegen neuere OpenSSL-Versionen kompiliert wird, behoben">
<correction python-flask-rdf "Fehlende Abhängigkeiten von python3-flask-rdf nachgetragen">
<correction python-pgmagick "Versionserkennung von graphicsmagick-Sicherheitsaktualisierungen, die sich selbst als Version 1.4 identifizieren, handhaben">
<correction python-werkzeug "Sicherstellen, dass Docker-Container eindeutige Fehlersuch-PINs haben [CVE-2019-14806]">
<correction ros-ros-comm "Problem mit Pufferüberlauf behoben [CVE-2019-13566]; Ganzzahlüberlauf behoben [CVE-2019-13445]">
<correction ruby-encryptor "Testfehlschläge ignorieren, um Kompilierungsfehlschläge zu beheben">
<correction rust-cbindgen "Neues Paket, um Firefox-ESR-Rückportierungen zu unterstützen">
<correction rustc "Neue Version der Originalautoren, um Firefox-ESR-Rückportierungen zu unterstützen">
<correction safe-rm "Installation in (und dadurch Beschädigen von) zusammengeführten /usr-Umgebungen verhindern">
<correction sorl-thumbnail "pgmagick-Ausnahme umgangen">
<correction sssd "sysdb: Suchfilter-Eingabe bereinigen [CVE-2017-12173]">
<correction tigervnc "Sicherheitsaktualisierungen [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Sicherheitskorrekturen [CVE-2014-6053 2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681 CVE-2019-8287]">
<correction tmpreaper "<q>--protect '/tmp/systemd-private*/*'</q> dem Cronjob hinzufügen, um Ausfälle von systemd-Diensten, die PrivateTmp=true haben, zu verhindern">
<correction tzdata "Neue Veröffentlichung der Originalautoren">
<correction ublock-origin "Neue Version der Originalautoren, kompatibel mit Firefox ESR 68">
<correction unhide "Stack-Erschöpfung behoben">
<correction x2goclient "~/, ~user{,/}, ${HOME}{,/} und $HOME{,/} von den Zielpfaden im SCP-Modus entfernen; behebt Regression mit neueren libssh-Versionen, die Behebungen für CVE-2019-14889 enthalten">
<correction xml-security-c "<q>DSA verification crashes OpenSSL on invalid combinations of key content</q> behoben">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Oldstable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheits-Team hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>


<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2019 4474 firefox-esr>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4509 apache2>
<dsa 2019 4509 subversion>
<dsa 2019 4511 nghttp2>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4522 faad2>
<dsa 2019 4523 thunderbird>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4528 bird>
<dsa 2019 4529 php7.0>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux>
<dsa 2019 4532 spip>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4537 file-roller>
<dsa XXXX 4539 openssl>
<dsa 2019 4540 openssl1.0>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4548 openjdk-8>
<dsa XXXX 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4552 php7.0>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4557 libarchive>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4564 linux>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4571 thunderbird>
<dsa 2019 4573 symfony>
<dsa 2019 4574 redmine>
<dsa 2019 4576 php-imagick>
<dsa 2019 4578 libvpx>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4587 ruby2.3>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4594 openssl1.0>
<dsa 2019 4595 debian-lan-config>
<dsa 2019 4596 tomcat8>
<dsa XXXX 4596 tomcat-native>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4607 openconnect>
<dsa 2020 4609 python-apt>
<dsa XXXX 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4614 sudo>
<dsa 2020 4615 spamassassin>
</table>


<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction firetray "Inkompatibel mit derzeitigen Thunderbird-Versionen">
<correction koji "Sicherheitsprobleme">
<correction python-lamson "Defekt wegen Änderungen in python-daemon">
<correction radare2 "Sicherheitsprobleme; Originalautoren bieten keine Stable-Unterstützung">
<correction ruby-simple-form "Ungenutzt, Sicherheitsprobleme">
<correction trafficserver "Nicht unterstützbar">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Oldstable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständigen Listen von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Oldstable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail an 
&lt;press@debian.org&gt;, oder kontaktieren das Stable-Release-Team 
auf Englisch über &lt;debian-release@lists.debian.org&gt;.</p>
