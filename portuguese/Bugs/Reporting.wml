#use wml::debian::template title="Debian BTS - relatando bugs" NOHEADER=yes NOCOPYRIGHT=true
#use wml::debian::translation-check translation="387a1b1ba7d55da8110abcaf96b5711187791f78"

<h1>Como relatar um bug no Debian usando o reportbug</h1>

<a name="reportbug"></a>
<p>Nós recomendamos fortemente que você reporte bugs no Debian usando o
programa <code>
<a href="https://packages.debian.org/stable/utils/reportbug">reportbug</a></code>.
</p>

<p>
O reportbug é instalado por padrão na maior parte dos sistemas.
Se não estiver disponível, ele pode ser instalado usando o
sistema de administração de pacotes do seu sistema.
</p>

<p>
O reportbug pode ser iniciado pelo menu, no submenu "sistema", ou
executado na linha de comando por <code>reportbug</code>.
</p>

<p>Ele guiará você, passo a passo, no processo de reportar um bug.</p>

<p>Se você tiver questões que o processo interativo do reportbug não
resolva, você pode consultar o restante da documentação abaixo ou perguntar na
<a href="mailto:debian-user-portuguese@lists.debian.org">lista de discussão dos(as)
usuários(as) Debian</a> (em português) ou para a versão em inglês da <a
href="mailto:debian-user@lists.debian.org">lista de discussão dos(as) usuários(as)
Debian</a>.</p>


<h1>Como relatar um bug no Debian usando o e-mail
   (e uso avançado do reportbug)</h1>

<h2>Pontos importantes a serem considerados <strong>antes</strong> de
enviar seu relatório de bug</h2>

<a name="whatpackage"></a>
<h3>A qual pacote o seu relatório de bug pertence?</h3>
<p>Você precisa saber a qual pacote o seu relatório de bug deve
ser endereçado. Veja <a href="#findpkgver">este exemplo</a> para
instruções sobre como encontrar esta informação. (Você usará
esta informação para <a href="#filedalready">verificar se seu
relatório de bug já foi relatado</a>).
</p>

<p>Se você não é capaz de determinar para qual pacote o seu relatório de bug
deve ser endereçado, por favor envie uma mensagem para a
<a href="mailto:debian-user-portuguese@lists.debian.org">lista de discussão dos(as)
usuários(as) Debian</a> (em português) ou para a versão em inglês da <a
href="mailto:debian-user@lists.debian.org">lista de discussão dos(as) usuários(as)
Debian</a> pedindo aconselhamento.</p>

<p>Se o seu problema não está relacionado somente a um pacote, mas a algum
serviço geral do Debian, existem diversos
<a href="pseudo-packages">pseudopacotes</a> ou mesmo
<a href="../MailingLists/">listas de discussão</a> que
você pode usar para nos transmitir sua mensagem.</p>

<a name="filedalready"></a>
<h3>Seu relatório de bug já foi relatado?</h3>
<p>Você deve verificar se seu relatório de bug já foi relatado antes
de enviá-lo. Você pode ver quais bugs de um pacote específico foram
relatados usando a
<a href="./#pkgreport">opção "pacote" no formulário de busca de bugs</a>.
Se há um relatório de bug existente #<var>&lt;número&gt;</var>,
você deve mandar seus comentários através de um e-mail para
<var>&lt;número&gt;</var>@bugs.debian.org em vez de relatar um novo bug.</p>

<h3>Enviando múltiplos relatórios para múltiplos bugs</h3>
<p>Por favor, não relate múltiplos bugs não relacionados &mdash; especialmente
os que são de pacotes diferentes &mdash; em um único relatório de bug.</p>

<h3>Não relate bugs para o(a) autor(a) (upstream)</h3>
<p>Se você relatar um bug no Debian, não envie uma cópia para os(as)
mantenedores(as) originais do software por conta própria, pois é possível que o
bug exista apenas no Debian. Se necessário, o(a) mantenedor(a) do pacote
encaminhará o bug para os(as) mantenedores(as) principais.</p>

<h2>Enviando o relatório de bug via e-mail</h2>

<p>Você pode relatar bugs no Debian enviando um e-mail para
<a href="mailto:submit@bugs.debian.org"><code>submit@bugs.debian.org</code></a>
com um formato especial descrito abaixo. O <code>reportbug</code> (<a
href="#reportbug">veja acima</a>) formatará os e-mails apropriadamente para
você; por favor, use-o!</p>

<h3>Cabeçalhos</h3>
<p>Como qualquer e-mail, você deve incluir uma linha de
<code>Assunto</code> clara e descritiva no cabeçalho principal.
O assunto que você definir será usado como o título inicial
do bug no sistema de acompanhamento de bugs (BTS - Bug Tracking System),
portanto escreva algo informativo!</p>

<p>Se você quiser enviar uma cópia de seu relatório de bug para
destinatários adicionais (como listas de discussão), você não deverá
usar os cabeçalhos de e-mail tradicionais, mas
<a href="#xcc">um método diferente, descrito abaixo</a>.</p>

<h3><a name="pseudoheader">Pseudocabeçalhos</a></h3>
<p>A primeira parte do relatório de bug são os pseudocabeçalhos, que
contêm informação sobre quais pacotes e versões aplicam-se ao seu relatório de
bug. A primeira linha do corpo da mensagem tem que incluir um
pseudocabeçalho. Ela deve mencionar:</p>

<pre>
Package: &lt;nome-do-pacote&gt;
</pre>

<p>Substitua <code>&lt;nome-do-pacote&gt;</code> pelo
<a href="#whatpackage">nome do pacote</a> que possui o bug.</p>

<p>A segunda linha da mensagem deve conter:</p>

<pre>
Version: &lt;versão-do-pacote&gt;
</pre>

<p>Substitua <code>&lt;versão-do-pacote&gt;</code> pela versão do pacote.
Por favor, não inclua aqui qualquer texto que não seja a própria
versão, já que o sistema de acompanhamento de bugs se baseia neste
campo para determinar quais <q>releases</q> (versões) são afetadas pelo bug.</p>

<p>Você precisa informar uma linha <code>Package</code> correta no
pseudocabeçalho para que o sistema de acompanhamento de bugs entregue
a mensagem para o(a) mantenedor(a) do pacote. Consulte
<a href="#findpkgver">este exemplo</a> para orientações sobre como
encontrar essa informação.</p>

<p>Para outros pseudocabeçalhos válidos, veja <a
href="#additionalpseudoheaders">Pseudocabeçalhos adicionais</a>.</p>

<h3>O corpo do relatório</h3>
<p>Por favor, inclua em seu relatório:</p>

<ul>
<li>O texto <em>exato</em> e <em>completo</em> de quaisquer mensagens
impressas ou gravadas (log). Isto é muito importante!</li>
<li>Exatamente o que você digitou ou fez para demonstrar o problema.</li>
<li>Uma descrição do comportamento incorreto: exatamente qual
comportamento você estava esperando e o que você observou. Uma transcrição
de uma sessão de exemplo é uma boa maneira de mostrar isso.</li>
<li>Uma correção sugerida, ou até mesmo um patch, caso você tenha preparado
um.</li>
<li>Detalhes da configuração do programa com problema. Inclua o texto
completo dos arquivos de configuração do programa em questão.</li>
<li>As versões de quaisquer pacotes dos quais o pacote com problema
dependa.</li>
<li>Qual versão de kernel você está usando (digite <code>uname -a</code>),
sua biblioteca C compartilhada (digite <code>ls -l /lib/*/libc.so.6</code> ou
<code>apt show libc6 | grep ^Version</code>) e quaisquer outros detalhes sobre
seu sistema Debian, caso pareçam apropriados. Por exemplo, se você tiver um
problema com um script Perl, você pode informar a versão do binário "perl"
(digite <code>perl -v</code> ou
<code>dpkg -s perl | grep ^Version:</code>).</li>
<li>Detalhes apropriados do hardware em seu sistema. Caso você esteja
relatando um problema com um controlador de dispositivo (device driver),
por favor liste <em>todo</em> hardware em seu sistema, uma vez que esses
problemas são normalmente causados por conflitos de endereços de I/O e IRQ.</li>
<li>Se você tem o <a
href="https://packages.debian.org/stable/utils/reportbug">reportbug</a>
 instalado, a saída de
 <code>reportbug --template -T none -s none -S normal -b --list-cc
 none -q &lt;pacote&gt;</code>
também será útil, pois contém a saída dos scripts de mantenedor(a) específicos
e informação de versão.</li>
</ul>

<p>Inclua qualquer detalhe que pareça relevante &mdash; você não perde nada por
fazer um longo relatório, então adicione muita informação. Se forem relatórios
pequenos, por favor inclua quaisquer arquivos que você esteja usando para
reproduzir o problema. (Se eles forem grandes, considere torná-los disponíveis
em algum site web publicamente acessível, se possível).</p>

<p>Para maiores informações sobre como ajudar os(as) desenvolvedores(as) a
solucionar seu problema, por favor, leia o documento
<a href="https://www.chiark.greenend.org.uk/~sgtatham/bugs.html">como relatar
bugs de maneira eficiente</a> (em inglês).</p>


<h2><a name="example">Um exemplo de relatório de bug</a></h2>

<p>Um relatório de bug, com o cabeçalho e o pseudocabeçalho, se assemelha a
este:</p>

<pre>
  To: submit@bugs.debian.org
  From: diligent@testing.linux.org
  Subject: Hello diz "goodbye"

  Package: hello
  Version: 1.3-16

  Quando eu invoco "hello" sem argumentos a partir de um shell comum
  o mesmo imprime "goodbye" ao invés do esperado "hello, world".
  Aqui está uma transcrição:

  $ hello
  goodbye
  $ /usr/bin/hello
  goodbye
  $

  Eu sugiro que a string de saída, em hello.c, seja corrigida.

  Estou usando GNU/Linux Debian 2.2, kernel 2.2.17-pre-patch-13
  e libc6 2.1.3-10.
</pre>
## Trecho somente para a versão em português
## (Excerpt for the portuguese version)
  <p>Lembre-se de escrever seu relatório de bug em inglês. Se precisar de
  ajuda, você pode enviar uma mensagem para
  <email "debian-devel-portuguese@lists.debian.org"> ou para
  <email "debian-l10n-portuguese@lists.debian.org">.</p>
## (/português)
## (/portuguese)

<h2><a name="xcc">Enviando cópias de relatórios de bugs para outros
endereços</a></h2>

<p>Algumas vezes é necessário enviar uma cópia do relatório de bug para
algum outro endereço além de <code>debian-bugs-dist</code> e
do(a) mantenedor(a) do pacote, que são os endereços para os quais normalmente se
envia.</p>

<p>Você poderia fazer isso enviando uma cópia de seu relatório de bug (usando
o campo CC) para o(s) outro(s) endereço(s). Mas desse modo as outras cópias
não teriam o número do relatório de bug no campo <code>Reply-To</code> e
na linha <code>Assunto</code>.
Quando os destinatários responderem, eles provavelmente preservarão a
entrada <code>submit@bugs.debian.org</code> no cabeçalho e terão suas
mensagens enviadas como um novo relatório de bug. Isto leva a muitos
relatórios duplicados.</p>

<p>A maneira <em>correta</em> de se fazer isso é usar o cabeçalho
<code>X-Debbugs-CC</code>. Adicione uma linha como essa ao cabeçalho
de sua mensagem:</p>
<pre>
 X-Debbugs-CC: outra-lista@cosmic.edu
</pre>
<p>Isto fará com que o sistema de acompanhamento de bugs envie uma cópia
de seu relatório para o(s) endereço(s) na linha <code>X-Debbugs-CC</code>
e também para <code>debian-bugs-dist</code>.</p>

<p>Se quiser enviar cópias para mais de um endereço, adicione-os numa única
linha <code>X-Debbugs-CC</code>, com o cuidado de separá-los por vírgulas.

<p>Evite enviar estas cópias para o endereço de outros relatórios de bugs,
já que elas serão pegas pela checagem que evita loops de mensagens. De qualquer
forma, não há um bom motivo em usar <code>X-Debbugs-CC</code> para isto,
já que o número do bug adicionado por este mecanismo será substituído por um
outro; em vez disso, use o campo <code>CC</code>.</p>

<p>Este recurso pode geralmente ser combinado de maneira útil com
<code>quiet</code> &mdash; veja abaixo.</p>

<a name="additionalpseudoheaders"></a>
<h1>Pseudocabeçalhos adicionais</h1>

<h2><a name="severities">Níveis de severidade</a></h2>

<p>Se um relatório é de um bug particularmente sério (serious), ou
é meramente uma requisição de novo recurso (wishlist), você pode definir o nível de
severidade no momento em que o relata. Isto, no entanto, não é exigido e
os(as) mantenedores(as) do pacote atribuirão um nível de severidade apropriado
para seu relatório caso você não o faça (ou escolha a severidade errada).</p>

<p>Para atribuir um nível de severidade, coloque uma linha como essa no
<a href="#pseudoheader">pseudocabeçalho</a>:</p>

<pre>
Severity: &lt;<var>severidade</var>&gt;
</pre>

<p>Substitua &lt;<var>severidade</var>&gt; por um dos níveis de severidade
disponíveis, como descrito na
<a href="Developer#severities">documentação avançada</a>.</p>

<h2><a name="tags">Atribuindo tags</a></h2>

<p>Você pode definir tags em um bug quando envia o relatório. Por exemplo,
caso você esteja incluindo um patch junto ao seu relatório de bug, você
pode definir a tag <code>patch</code>. Porém, isso não é
exigido, e os(as) desenvolvedores(as) definirão tags em seu relatório quando
apropriado.</p>

<p>Para definir tags, inclua uma linha como essa no
<a href="#pseudoheader">pseudocabeçalho</a>:</p>

<pre>
Tags: &lt;<var>etiquetas</var>&gt;
</pre>

<p>Substitua &lt;<var>etiquetas</var>&gt; por uma ou mais tags disponíveis, conforme
descrito na <a href="Developer#tags">documentação avançada</a>.
Separe múltiplas tags por vírgulas, espaços ou ambos.</p>

<pre>
User: &lt;<var>nome-de-usuário(a)</var>&gt;
Usertags: &lt;<var>tags-de-usuário(a)</var>&gt;
</pre>

<p>Substitua &lt;<var>tags-de-usuário(a)</var>&gt; com uma ou mais tags de usuário(a).
Separe várias tags com vírgulas, espaços ou ambos. Se você especificar um
&lt;<var>nome-de-usuário(a)</var>&gt;, as tags deste(a) usuário(a) serão definidas.
Caso contrário, o endereço de e-mail do remetente será usado como nome de
usuário(a).</p>

<p>Você pode definir tags-de-usuário(a) para vários(as) usuários(as) no momento do
envio do bug, incluindo vários pseudocabeçalhos de usuário(a); cada
tag-de-usuário(a) pseudocabeçalho define as tags-de-usuário(a) para o
pseudocabeçalho do(a) usuário(a) anterior. Isto é especialmente útil para definir
tags-de-usuário(a) para uma equipe com vários(as) usuários(as), definir tags-de-usuário(a)
para várias equipes ou definir as
<a href="https://wiki.debian.org/Teams/Debbugs/ArchitectureTags">https://wiki.debian.org/Teams/Debbugs/ArchitectureTags|tags-de-usuário(a) architecture</a>
para bugs que afetam várias arquiteturas .
</p>

<pre>
User: &lt;<var>primeiro-nome-do(a)-usuário(a)</var>&gt;
Usertags: &lt;<var>tags-de-usuário(a) primeiro-nome-do(a)-usuário(a)</var>&gt;
User: &lt;<var>segundo-nome-do(a)-usuário(a)</var>&gt;
Usertags: &lt;<var>tags-de-usuário(a) segundo-nome-do(a)-usuário(a)</var>&gt;
</pre>

<h2>Definir encaminhamento</h2>
<pre>
Forwarded: <var>foo@example.com</var>
</pre>

<p>
marcará o novo relatório de bug submetido como encaminhado para
foo@example.com. Veja, para detalhes,
<a href="Developer#forward">registrando que você encaminhou um relatório de bug</a>
na documentação dos(as) desenvolvedores(as).
</p>

<h2>Reivindicar responsabilidade</h2>
<pre>
Owner: <var>foo@example.com</var>
</pre>

<p>
indicará que foo@example.com é agora o(a) responsável por resolver este bug.
Veja <a href="Developer#owner">alterando o responsável pelo bug</a> na
documentação dos(as) desenvolvedores(as) para detalhes.
</p>


<h2>Pacote de código fonte</h2>
<pre>
Source: <var>foopacote</var>
</pre>

<p>
o equivalente a <code>Package:</code> para bugs presentes no pacote do código fonte
de foopacote; para a maioria dos bugs na maioria dos pacotes você não precisa
usar esta opção.
</p>

<h2><a name="control">Comandos de Controle</a></h2>
<pre>
Control: <var>comandos de controle</var>
</pre>

<p>
Permite que quaisquer comandos que devem ser enviados para
<code>control@bugs.debian.org</code> funcionem quando enviados para
<code>submit@bugs.debian.org</code> ou <code>nnn@bugs.debian.org</code>.
    -1 refere-se inicialmente ao bug atual (ou seja, o bug criado por um
    e-mail para submit@ ou o bug comunicado com nnn@). Por favor, veja a
    <a href="server-control">documentação de servidor de controle</a> para mais
    informações sobre os comandos de controle válidos.</p>

<p>Por exemplo, o pseudocabeçalho na mensagem enviada
  para <code>12345@bugs.debian.org</code>:</p>

<pre>
Control: retitle -1 este é um título
Control: severity -1 normal
Control: summary -1 0
Control: forward -1 https://bugs.debian.org/nnn
</pre>

<p>faz com que o bug 12345 receba um novo título (em inglês), mude sua severidade,
defina um sumário e marque o bug como encaminhado.</p>



<h2>Cabeçalhos X-Debbugs-</h2>
<p>Finalmente, se seu
<acronym title="Mail User Agent" lang="en">MUA</acronym>
não permite que você edite os cabeçalhos das mensagens, você pode
definir os diversos cabeçalhos <code>X-Debbugs-</code> nos
<a href="#pseudoheader">pseudocabeçalhos</a>.</p>


<h1>Informação adicional</h1>

<h2>Endereços de envio diferentes (relatórios de bugs menores ou em massa)</h2>

<p>Caso um relatório seja de um bug menor, por exemplo um erro ortográfico na
documentação ou um problema de compilação trivial, por favor ajuste a
severidade apropriadamente e envie o mesmo para
<code>maintonly@bugs.debian.org</code> em vez de
<code>submit@bugs.debian.org</code>.
<code>maintonly</code> encaminhará o relatório somente para o(a) mantenedor(a)
do pacote e não para as listas de discussão do BTS.</p>

<p>Caso você esteja enviando muitos relatórios de uma vez, você definitivamente
deverá usar <code>maintonly@bugs.debian.org</code> de modo que não
cause muito tráfego redundante nas listas de discussão do BTS. Antes de enviar
muitos bugs similares, você também pode publicar um sumário em
<code>debian-bugs-dist</code>.</p>

<p>Caso deseje relatar um bug para o sistema de acompanhamento de bugs que
já tenha sido enviado para o(a) mantenedor(a), você pode usar
<code>quiet@bugs.debian.org</code>. Bugs enviados para
<code>quiet@bugs.debian.org</code> não
serão encaminhados para lugar algum, somente serão registrados.</p>

<p>Quando você utiliza endereços de envio diferentes, o sistema de
acompanhamento de bugs irá definir o <code>Reply-To</code> de quaisquer
mensagens encaminhadas para que as respostas sejam por padrão processadas
da mesma maneira que o relatório original. Isto significa que, por exemplo,
respostas para <code>maintonly</code> irão para
<var>nnn</var><code>-maintonly@bugs.debian.org</code> em vez de
<var>nnn</var><code>@bugs.debian.org</code>, a menos, é claro, que alguém
sobrescreva isso manualmente.</p>


<h2>Confirmações</h2>

<p>Normalmente, o sistema de acompanhamento de bugs lhe retornará uma
confirmação por e-mail, após você relatar um novo bug ou enviar uma informação
adicional para um bug existente. Se você quiser suprimir esta confirmação,
inclua um cabeçalho ou pseudocabeçalho <code>X-Debbugs-No-Ack</code> no seu
e-mail (o conteúdo deste cabeçalho não importa). Se você relatar um novo bug com
este cabeçalho, você precisará checar a interface web para descobrir o número do
bug.</p>

<p>Note que este cabeçalho não irá suprimir confirmações do servidor de e-mails
<code>control@bugs.debian.org</code>, já que estas confirmações podem conter
mensagens de erro que devem ser lidas e trabalhadas imediatamente.</p>

<h2>Bloqueio de spam e e-mails perdidos</h2>

<p>O sistema de acompanhamento de bugs implementa um conjunto particularmente
   abrangente de regras para se certificar de que não seja inserido spam no BTS.
   Mesmo que tentemos minimizar o número de falsos positivos, eles ocorrem.
   Se você suspeitar de que seu e-mail causou um falso positivo, fique à vontade
   para entrar em contato com <code>owner@bugs.debian.org</code> e pedir
   ajuda. Outra causa comum pela qual um e-mail não chega ao BTS é a utilização
   de endereços que coincidem com FROM_DAEMON do procmail, o que inclui
   endereços como <code>mail@foobar.com</code>. Se você suspeitar que seu e-mail
   coincide com FROM_DAEMON, veja
   <a href="https://manpages.debian.org/cgi-bin/man.cgi?query=procmailrc">procmailrc(5)</a>
   para verificar, e então re-envie seu e-mail usando um endereço que não coincida
   com FROM_DAEMON.</p>


<h2>Relatórios de bugs para pacotes desconhecidos</h2>

<p>Caso o sistema de acompanhamento de bugs não saiba quem é o(a) mantenedor(a)
do pacote em questão, o mesmo encaminhará o relatório para
<code>debian-bugs-dist</code> mesmo se <code>maintonly</code> for usado.</p>

<p>Ao enviar para <code>maintonly@bugs.debian.org</code> ou
<var>nnn</var><code>-maintonly@bugs.debian.org</code>, você deve certificar-se
de atribuir o relatório de bug ao pacote correto, colocando um
<code>Package</code> correto no topo de um envio original de um relatório, ou
usando o <a href="server-control">serviço <code>control@bugs.debian.org</code></a>
para (re)atribuir o relatório apropriadamente.</p>

<h2><a name="findpkgver">Usando o <code>dpkg</code> para encontrar o pacote e a
versão para o relatório</a></h2>

<p>Quando estiver usando o <code>reportbug</code> para relatar um bug de
um comando, digamos o <code>grep</code>, a instrução a seguir selecionará
automaticamente o pacote correto e permitirá que imediatamente você escreva o
relatório: <code>reportbug --file $(which grep)</code></p>

<p>Você também pode encontrar qual pacote instalou o comando através de
<code>dpkg --search</code>. Você pode encontrar qual versão instalada de um
pacote você possui usando <code>dpkg --list</code> ou <code>dpkg --status</code>.
</p>

<p>Por exemplo:</p>
<pre>
$ which apt-get
/usr/bin/apt-get
$ type apt-get
apt-get is /usr/bin/apt-get
$ dpkg --search /usr/bin/apt-get
apt: /usr/bin/apt-get
$ dpkg --list apt
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Installed/Config-files/Unpacked/Failed-config/Half-installed
|/ Err?=(none)/Hold/Reinst-required/X=both-problems (Status,Err: uppercase=bad)
||/ Name           Version        Description
+++-==============-==============-============================================
ii  apt            0.3.19         Advanced front-end for dpkg
$ dpkg --status apt
Package: apt
Status: install ok installed
Priority: standard
Section: base
Installed-Size: 1391
Maintainer: APT Development Team &lt;deity@lists.debian.org&gt;
Version: 0.3.19
Replaces: deity, libapt-pkg-doc (&lt;&lt; 0.3.7), libapt-pkg-dev (&lt;&lt; 0.3.7)
Provides: libapt-pkg2.7
Depends: libapt-pkg2.7, libc6 (&gt;= 2.1.2), libstdc++2.10
Suggests: dpkg-dev
Conflicts: deity
Description: Advanced front-end for dpkg
 This is Debian's next generation front-end for the dpkg package manager.
 It provides the apt-get utility and APT dselect method that provides a
 simpler, safer way to install and upgrade packages.
 .
 APT features complete installation ordering, multiple source capability
 and several other unique features, see the Users Guide in
 /usr/doc/apt/guide.text.gz

</pre>

<a name="otherusefulcommands"></a>
<h2>Outros comandos e pacotes úteis</h2>

<p>
A ferramenta <kbd>querybts</kbd>, disponível a partir do mesmo pacote que o
<a href="https://packages.debian.org/stable/utils/reportbug">reportbug</a>,
fornece uma interface conveniente em modo texto para o sistema de
acompanhamento de bugs.</p>

<p>Usuários Emacs também podem usar o comando debian-bug fornecido pelo pacote
<code><a href="https://packages.debian.org/stable/utils/debian-el">\
debian-el</a></code>. Quando chamado com <kbd>M-x debian-bug</kbd>,
ele perguntará por todas as informações necessárias de forma similar ao
<code>reportbug</code>.</p>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"
