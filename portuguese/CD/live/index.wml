#use wml::debian::cdimage title="Imagens de instalação live"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="8c86ac02236495359e82eed0e3b9e29905514fd7"

<p>Uma imagem de <q>instalação live</q> contém um sistema Debian que pode
inicializar sem modificar qualquer arquivo no disco rígido e também
permite a instalação do Debian a partir do conteúdo da imagem.
</p>

<p><a name="choose_live"><strong>Uma imagem <q>live</q> é adequada para mim?</strong></a>
Aqui estão algumas coisas a se considerar que o ajudarão a decidir.
<ul>
<li><b>Tipos:</b> As imagens <q>live</q> vêm com vários tipos, proporcionando
uma escolha de ambientes de área de trabalho (GNOME, KDE, LXDE, Xfce,
Cinnamon e MATE). Muitos(as) usuários(as) vão considerar essa seleção inicial de pacotes
adequada, podendo instalar qualquer pacote adicional que precisarem
depois pela rede.
<li><b>Arquitetura:</b> Somente imagens para as duas arquiteturas mais
populares, PC de 32 bits (i386) e PC de 64 bits (amd64), são atualmente
fornecidas.
<li><b> Instalador:</b> a partir do Debian 10 Buster, as imagens live contêm o
amigável ao usuário final <a href="https://calamares.io">instalador Calamares</a>,
um framework de instalação independente de distribuição, como alternativa ao
nosso conhecido <a href="$(HOME)/devel/debian-installer">Debian-Installer</a>.
<li><b>Tamanho:</b> Cada imagem é muito menor que o conjunto completo de
imagens de DVD, mas é maior que uma mídia de instalação via rede.
<li><b>Idiomas:</b> As imagens não contém um conjunto completo de pacotes de
suporte à idiomas. Se você precisa de métodos de entrada, fontes e pacotes
adicionais para seu idioma, você precisará instalá-los depois.
</ul>

<p>As seguintes imagens de instalação <q>live</q> estão disponíveis para baixar:</p>

<ul>

  <li>Imagens oficiais de <q>instalação live</q> para a versão <q>estável (stable)</q>
  &mdash; <a href="#live-install-stable">veja abaixo</a></li>

</ul>


<h2 id="live-install-stable">Imagens oficiais de instalação <q>live</q> para a versão <q>estável</q></h2>

<p>Oferecida em diferentes tipos, cada uma diferindo no tamanho como explicado
acima, essas imagens são adequadas para testar um sistema Debian, composta de
um conjunto padrão selecionado de pacotes, e então instalá-lo a partir da mesma
mídia.</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (via <a
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p>Arquivos de imagem ISO <q>híbridas</q> adequadas para gravar em mídias de
DVD-R(W), e também em pendrives USB de tamanho apropriado. Se você pode usar
BitTorrent, por favor, faça-o, pois ele reduz a carga em nossos servidores.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"> <p><strong>DVD/USB</strong></p>
<p>Arquivos de imagem ISO <q>híbridas</q> adequadas para gravar em mídias de
DVD-R(W), e também em pendrives USB de tamanho apropriado.</p>
	  <stable-live-install-iso-cd-images />
</div> </div>

<p>Para informações sobre o que são estes arquivos e como usá-los, por favor,
veja a <a href="../faq/">FAQ</a>.</p>

<p>Se você pretende instalar o Debian a partir da imagem <q>live</q> baixada,
certifique-se de dar uma olhada nas
<a href="$(HOME)/releases/stable/installmanual">informações
detalhadas sobre o processo de instalação</a>.</p>

<p>Veja a <a href="$(HOME)/devel/debian-live">página do Projeto Debian Live</a>
para mais informações sobre os sistemas Debian Live fornecidos por essas
imagens.</p>

# Tradutores: o seguinte parágrafo existe (nesta ou em uma forma semelhante) várias vezes nos webwml,
# portanto, tente manter as traduções consistentes. Veja:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Imagens live não oficiais de CD/DVD com firmwares não livres incluídos</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Se algum hardware em seu sistema <strong>requer que firmware não livre seja
carregado</strong> com o controlador do dispositivo, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
arquivos tarball de pacotes de firmware comuns</a> ou baixar uma imagem
<strong>não oficial</strong> que inclui esses firmwares <strong>não livres</strong>.
Instruções de como usar os arquivos tarball e informações gerais sobre como
carregar um firmware durante uma instalação podem ser encontradas no
<a href="../releases/stable/amd64/ch06s04">guia de instalação</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/">imagens
de instalação não oficiais para <q>estável (stable)</q> com firmwares
incluídos</a>
</p>
</div>
