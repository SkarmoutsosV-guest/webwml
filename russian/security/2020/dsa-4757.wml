#use wml::debian::translation-check translation="2592e40c5d7143a6f575ff96f6127ba4fb3f18d5" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В HTTPD-сервере Apache было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1927">CVE-2020-1927</a>

    <p>Фабрис Перез сообщил, что некоторые конфигурации mod_rewrite
    уязвимы к открытому перенаправлению.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1934">CVE-2020-1934</a>

    <p>Шамаль де Сильва обнаружил, что модуль mod_proxy_ftp использует
    неинициализированную память при запуске прокси к вредоносному FTP-движку.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9490">CVE-2020-9490</a>

    <p>Феликс Вильгельм обнаружил, что специально сформированное значение для заголовка
    'Cache-Digest' в запросе HTTP/2 может вызывать аварийную остановку, если после этого
    сервер действительно пытается выполнить HTTP/2 PUSH ресурса.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11984">CVE-2020-11984</a>

    <p>Феликс Вильгельм сообщил о переполнении буфера в модуле mod_proxy_uwsgi,
    которое может приводить к раскрытию информации или удалённому выполнению
    кода.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11993">CVE-2020-11993</a>

    <p>Феликс Вильгельм сообщил, что при включении трассировки/отладки для модуля
    HTTP/2 некоторые виды трафика могут вызывать запись журнала на неправильных
    соединениях, что приводит к параллельному использованию пулов
    памяти.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2.4.38-3+deb10u4.</p>

<p>Рекомендуется обновить пакеты apache2.</p>

<p>С подробным статусом поддержки безопасности apache2 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4757.data"
