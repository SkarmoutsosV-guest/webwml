# Translation of mailinglists.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2013-05-14 13:14+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Prihlásenie do konferencie"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Informácie o tom ako sa prihlásiť pomocou emailu nájdete na stránke <a href="
"\"./#subunsub\">konferencií</a>. Na odhlásenie sa z konferencií môžete "
"použiť aj <a href=\"unsubscribe\">webový formulár</a>. "

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Pamätajte, že väčšina konferencií Debianu sú verejné fóra. Každý email, "
"ktorý do nich pošlete bude uložený vo verejných archívov konferencie a budú "
"ho indexovať vyhľdávače. Do konferencií Debianu by ste mali posielať "
"príspevky iba z emailovej adresy, ktorej zverejnenie vám nevadí."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Prosím, vyberte, do ktorých konferencií sa chcete prihlásiť (počet pokusov o "
"prihlásenie je obmedzený - ak sa nepodarí vašu požiadavku spracovať, prosím, "
"použite <a href=\"./#subunsub\">inú metódu</a>):"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Nebol uvedený popis"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Moderovaná:"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Iba prihlásení členovia môžu posielať správy."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr "Táto konferencia prijíma iba správy, ktoré podpísali vývojári Debianu."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Prihlásenie:"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "zhrnutá verzia len na čítanie."

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "Vaša emailová adresa:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "Prihlásiť sa"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Vyčistiť"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Rešpektujte prosím <a href=\"./#ads\">politiku reklamy v konferenciách "
"Debianu</a>."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Odhlásenie z konferencie"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Informácie o tom ako sa odhlásiť pomocou emailu nájdete na stránke <a href="
"\"./#subunsub\">konferencií</a>. Na prihlásenie sa do konferencií môžete "
"použiť aj <a href=\"subscribe\">webový formulár</a>. "

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Prosím, vyberte, z ktorých konferencií sa chcete odhlásiť (počet pokusov o "
"odhlásenie je obmedzený, ak sa nepodarí vašu požiadavku spracovať, prosím, "
"použite <a href=\"./#subunsub\">inú metódu</a>):"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Odhlásiť sa"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "otvorené"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "zatvorené"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Prosím vyberte, do ktorých konferencií sa chcete prihlásiť:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Prosím vyberte, z ktorých konferencií sa chcete odhlásiť:"
