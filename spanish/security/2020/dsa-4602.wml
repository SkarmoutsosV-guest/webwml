#use wml::debian::translation-check translation="4c0996e191dd68b6dfbadfee3c048714d4cfa961"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto múltiples vulnerabilidades en el hipervisor Xen que
podrían dar lugar a denegación de servicio, a elevación de privilegios del huésped al anfitrión («guest-to-host») o a
fugas de información.</p>

<p>Adicionalmente, esta actualización proporciona mitigaciones para el ataque de canal lateral
especulativo <q>TSX Asynchronous Abort</q>. Para información adicional, consulte
<a href="https://xenbits.xen.org/xsa/advisory-305.html">https://xenbits.xen.org/xsa/advisory-305.html</a></p>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 4.8.5.final+shim4.10.4-1+deb9u12. Tenga en cuenta que esta será la
última actualización de seguridad de Xen en la distribución «antigua estable»; el soporte
del proyecto original para la rama 4.8.x terminó a finales de diciembre de 2019. Si
necesita soporte de seguridad para su instalación Xen le recomendamos que
la actualice a la distribución «estable» (buster).</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.11.3+24-g14b62ab3e5-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de xen.</p>

<p>Para información detallada sobre el estado de seguridad de xen, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/xen">https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4602.data"
